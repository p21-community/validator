/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import net.pinnacle21.validator.api.model.ValidationOptions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.regex.Pattern;

import static net.pinnacle21.validator.data.DataEntry.NULL_ENTRY;

public class DataEntryFactory {
    private static final ThreadLocal<Pattern> DATE_PATTERN = ThreadLocal.withInitial(() -> Pattern.compile(
        "^(?:-|[0-9]{4})" +
        "(?:-" +
        "(?:-|0[0-9]|1[0-2])" +
        "(?:-" +
        "(?:-|[0-2][0-9]|3[0-1])" +
        "(?:T" +
        "(?:-|[0-1][0-9]|2[0-4])" +
        "(?::" +
        "(?:-|[0-5][0-9])" +
        "(?::[0-5][0-9])?" +
        ")?" +
        ")?" +
        ")?" +
        ")?$"
    ));
    private final ThreadLocal<DecimalFormat> decimalPattern;
    private final int decimalCount;

    public DataEntryFactory(ValidationOptions options) {
        int decimalCount = 8;

        if (options.hasProperty("Engine.RoundingDigits")) {
            try {
                decimalCount = Integer.parseInt(options.getProperty("Engine.RoundingDigits"));
            } catch (NumberFormatException ignore) {}
        }

        this.decimalCount = decimalCount;
        this.decimalPattern = ThreadLocal.withInitial(() -> {
            DecimalFormat format = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

            format.setMaximumFractionDigits(this.decimalCount);

            return format;
        });
    }

    public DataEntry create(Object value) {
        if (value instanceof String) {
            value = rtrim((String)value);
        }

        if (value == null) {
            return NULL_ENTRY;
        }

        return new DataEntryImpl(value);
    }

    public int getDecimalCount() {
        return this.decimalCount;
    }

    private class DataEntryImpl implements DataEntry {
        private final Object value;
        private final DataType datatype;

        DataEntryImpl(Object value) {
            // TODO: Why do we do this? I'm keeping it like this to match the current behaviour but it seems odd
            int comparisonDigits = decimalCount - 2;

            if (value instanceof BigDecimal) {
                // TODO: Technically this may be an integer but since this isn't used anywhere we're avoiding the overhead
                this.datatype = DataType.Float;
                this.value = ((BigDecimal)value).setScale(comparisonDigits, RoundingMode.HALF_UP);

                return;
            }

            this.datatype = determineImplicitType(value);

            if (this.datatype.isNumeric()) {
                if (value instanceof Float) {
                    value = ((Float)value).doubleValue();
                }

                if (value instanceof Double) {
                    double converted = (Double)value;

                    if (converted == Math.rint(converted)) {
                        value = (long)converted;
                    }
                }

                BigDecimal converted;

                if (value instanceof String || value instanceof Double) {
                    if (value instanceof Double) {
                        value = decimalPattern.get().format((double)(Double)value);
                    }

                    converted = new BigDecimal((String)value);
                    converted = converted.setScale(comparisonDigits, RoundingMode.HALF_UP);
                } else if (value instanceof Long) {
                    converted = new BigDecimal((Long)value);
                } else if (value instanceof Integer) {
                    converted = new BigDecimal((Integer)value);
                } else {
                    // The assertion below is bogus since that will never happen, but the IDE gets mad we might cause a NPE (we can't)
                    assert value != null;

                    throw new IllegalArgumentException("Unable to handle provided value of type " + value.getClass().getName());
                }

                value = new NumericValue(converted, value.toString());
            }

            this.value = value;
        }

        public int compareToAny(Object o, boolean caseSensitive) {
            DataEntry rhsEntry = o instanceof DataEntry ? (DataEntry)o : create(o);

            int result;
            Object lhs = this.getValue();
            Object rhs = rhsEntry.getValue();
            DataType lhsType = this.getDataType();
            DataType rhsType = rhsEntry.getDataType();

            // Note that lhs cannot be null, or we wouldn't be in this method!

            // The implicit type is theoretically the most "natural" comparison we're going
            // to get, so we attempt to compare on those grounds first. If not, we'll default
            // to comparing strings.
            if (rhs == null) {
                result = 1;
            } else {
                if (lhsType.isNumeric() && rhsType.isNumeric()) {
                    result = ((BigDecimal)lhs).compareTo((BigDecimal)rhs);
                } else {
                    if (!(lhs instanceof String)) {
                        lhs = this.toString();
                    }

                    if (!(rhs instanceof String)) {
                        rhs = rhsEntry.toString();
                    }

                    String lhsValue = (String)lhs;
                    String rhsValue = (String)rhs;
                    boolean lhsDate = lhsType == DataType.DateTime ||
                            (lhsType == DataType.Integer && lhsValue.length() == 4);
                    boolean rhsDate = rhsType == DataType.DateTime ||
                            (rhsType == DataType.Integer && rhsValue.length() == 4);

                    // Perform fuzzy equality for things that look like dates
                    if (lhsDate && rhsDate && (lhsValue.startsWith(rhsValue) ||
                                               rhsValue.startsWith(lhsValue))) {
                        result = 0;
                    } else {
                        if (!caseSensitive) {
                            lhsValue = lhsValue.toUpperCase();
                            rhsValue = rhsValue.toUpperCase();
                        }

                        result = lhsValue.compareTo(rhsValue);
                    }
                }
            }

            return result;
        }

        @Override
        public int compareTo(DataEntry rhs) {
            DataType lhsType = this.datatype;
            DataType rhsType = rhs.getDataType();

            int lhsSort = lhsType.getSortOrder();
            int rhsSort = rhsType.getSortOrder();

            if (lhsSort != rhsSort) {
                return lhsSort - rhsSort;
            }

            if (lhsType.isNumeric()) {
                // TODO: This cast involves an assumption that isn't guaranteed anymore now that we broke DataEntry into an interface
                return ((BigDecimal)this.getValue()).compareTo((BigDecimal)rhs.getValue());
            }

            return this.getValue().toString().compareTo(rhs.getValue().toString());
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof DataEntry && this.compareTo((DataEntry)o) == 0;
        }

        public DataType getDataType() {
            return this.datatype;
        }

        public Object getValue() {
            return this.value instanceof NumericValue ?
                ((NumericValue)this.value).getValue() : this.value;
        }

        public boolean isNumeric() {
            return this.datatype.isNumeric();
        }

        public boolean isDate() {
            return this.datatype.isDate();
        }

        @Override
        public int hashCode() {
            return this.hasValue() ? this.getValue().hashCode() : 0;
        }

        public boolean hasValue() {
            return this.value != null;
        }

        @Override
        public String toString() {
            return this.value != null ? this.value.toString() : "";
        }
    }

    private static DataEntry.DataType determineImplicitType(Object value) {
        DataEntry.DataType type = null;

        if (value == null) {
            type = DataEntry.DataType.Null;
        } else if (value instanceof String) {
            String asString = (String)value;
            int length = asString.length();
            char firstChar = length > 0 ? asString.charAt(0) : '\0';

            if (length == 0 || !(((firstChar == '-' || firstChar == '+') && length > 1) ||
                    (firstChar == '.' && length > 1) ||
                    (firstChar >= '0' && firstChar <= '9'))) {
                // Can't be anything else
                type = DataEntry.DataType.Text;
            } else {
                boolean couldBeDouble  = firstChar == '.';
                boolean determinedType = true;
                boolean possibleNumber = firstChar != '0' || (length == 1 || asString.charAt(1) == '.');

                if (possibleNumber) {
                    for (int i = 1; i < length; ++i) {
                        char currentChar = asString.charAt(i);
                        if (!(currentChar >= '0' && currentChar <= '9')) {
                            if (currentChar == '.' && !couldBeDouble && i < length - 1) {
                                couldBeDouble = true;
                            } else {
                                determinedType = false;
                                break;
                            }
                        }
                    }
                } else {
                    determinedType = false;
                }

                if (determinedType) {
                    if (couldBeDouble) {
                        type = DataEntry.DataType.Float;
                    } else {
                        type = DataEntry.DataType.Integer;
                    }
                } else {
                    if (DATE_PATTERN.get().matcher(asString).matches()) {
                        type = DataEntry.DataType.DateTime;
                    } else {
                        type = DataEntry.DataType.Text;
                    }
                }
            }
        } else {
            if (value instanceof Integer || value instanceof Long) {
                type = DataEntry.DataType.Integer;
            } else if (value instanceof Float || value instanceof Double) {
                type = DataEntry.DataType.Float;
            }
        }

        return type;
    }

    private static String rtrim(String string) {
        int length = string.length();
        int i = length - 1;

        for ( ; i > -1; --i) {
            if (string.charAt(i) != ' ') {
                break;
            }
        }

        if (i == -1) {
            return null;
        }

        return i < length - 1 ? string.substring(0, i + 1) : string;
    }

    private static class NumericValue {
        private final BigDecimal value;
        private final String string;

        NumericValue(BigDecimal value, String string) {
            this.value = value;
            this.string = string;
        }

        BigDecimal getValue() {
            return this.value;
        }

        @Override
        public String toString() {
            return this.string;
        }
    }

    static class NullDataEntry implements DataEntry {
        @Override
        public int compareToAny(Object rhs, boolean caseSensitive) {
            if (rhs instanceof DataEntry) {
                rhs = ((DataEntry)rhs).getValue();
            }

            return rhs == null ? 0 : -1;
        }

        @Override
        public String toString() {
            return "";
        }

        @Override
        public int compareTo(DataEntry rhs) {
            return rhs.hasValue() ? -1 : 0;
        }

        @Override
        public DataType getDataType() {
            return DataType.Null;
        }

        @Override
        public Object getValue() {
            return null;
        }

        @Override
        public boolean isNumeric() {
            return false;
        }

        @Override
        public boolean isDate() {
            return false;
        }

        @Override
        public boolean hasValue() {
            return false;
        }
    }
}

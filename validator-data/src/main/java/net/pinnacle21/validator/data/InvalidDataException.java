/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

/**
 *
 * @author Tim Stone
 */
public class InvalidDataException extends Exception {
    private static final long serialVersionUID = -1542200475977752791L;

    /**
     *
     */
    public enum Codes {
        CannotParseSource,
        NoVariables,
        NoRecords,
        InvalidRequestState,
        DataDimensionMismatch,
        MissingSource,
        DuplicateVariable
    }

    private final Codes code;

    /**
     *
     * @param errorCode
     */
    public InvalidDataException(Codes errorCode) {
        this(errorCode, null);
    }

    /**
     *
     * @param errorCode
     * @param message
     */
    public InvalidDataException(Codes errorCode, String message) {
        super(message);

        this.code = errorCode;
    }

    /**
     *
     * @return
     */
    public Codes getCode() {
        return this.code;
    }
}

/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import net.pinnacle21.validator.api.model.VariableDetails;
import net.pinnacle21.validator.util.PropertySet;

/**
 * @author Tim Stone
 */
public class InternalVariableDetails extends PropertySet<VariableDetails.Property> implements VariableDetails {
    InternalVariableDetails(String name, Integer order) {
        super(VariableDetails.Property.class);

        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }

        if (order == null) {
            throw new IllegalArgumentException("order cannot be null");
        }

        this.setProperty(Property.Name, name);
        this.setProperty(Property.Order, order);
    }

    InternalVariableDetails(String name, Integer order, String type, Integer length, String label, String format,
            String fullFormat) {
        this(name, order);

        this.setProperty(Property.Type, type);
        this.setProperty(Property.Length, length);
        this.setProperty(Property.Label, label);
        this.setProperty(Property.Format, format);
        this.setProperty(Property.FullFormat, fullFormat);
    }
}

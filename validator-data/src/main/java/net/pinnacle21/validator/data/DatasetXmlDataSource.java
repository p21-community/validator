/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.api.model.SourceOptions;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author Tim Stone
 */
public class DatasetXmlDataSource extends AbstractDataSource {
    private final Map<String, String> oids = new LinkedHashMap<>();
    private XMLStreamReader reader;
    private final File define;
    private final File source;
    private String metadataOID;
    private String itemgroupOID;
    private boolean isTested;

    public DatasetXmlDataSource(SourceOptions options, DataEntryFactory factory) throws InvalidDataException {
        super(options, factory);

        File source = new File(options.getSource());
        File define;

        if (options.hasProperty("Define")) {
            define = new File(options.getProperty("Define"));
        } else {
            define = new File(source.getParentFile(), "define.xml");
        }

        if (!source.isFile() || !define.isFile()) {
            // Someone didn't do a good job at checking, so...
            throw new InvalidDataException(InvalidDataException.Codes.MissingSource,
                    "The source file is missing or is not a file");
        }

        this.source = source;
        this.define = define;
    }

    public DataSource replicate() throws InvalidDataException {
        return new DatasetXmlDataSource(super.options, this.factory);
    }

    public void close() {
        if (this.reader != null) {
            try {
                this.reader.close();
            } catch (XMLStreamException ignore) {}
        }
    }

    public boolean test() {
        boolean result = true;

        try {
            this.reader = XMLInputFactory.newInstance().createXMLStreamReader(
                new FileInputStream(this.source)
            );

            int event, depth = 0;

            while (this.reader.hasNext() && result) {
                event = this.reader.next();

                if (event == XMLStreamReader.START_ELEMENT) {
                    String tag = this.reader.getLocalName();

                    if (depth == 0 && !tag.equals("ODM")) {
                        result = false;
                    } else if (depth == 1) {
                        if (tag.equals("ReferenceData") || tag.equals("ClinicalData")) {
                            this.metadataOID = this.reader.getAttributeValue(
                                null, "MetaDataVersionOID"
                            );

                            if (this.metadataOID == null) {
                                result = false;
                            }
                        } else {
                            result = false;
                        }
                    } else if (depth == 2) {
                        if (tag.equals("ItemGroupData")) {
                            this.itemgroupOID = this.reader.getAttributeValue(
                                null, "ItemGroupOID"
                            );

                            if (this.itemgroupOID == null) {
                                result = false;
                            }

                            break;
                        } else {
                            result = false;
                        }
                    }

                    ++depth;
                }
            }
        } catch (XMLStreamException | IOException ex) {
            result = false;
        }

        this.isTested = true;

        return result;
    }

    // TODO: We should cache the define.xml data across instances so we can just read it once
    // But this has the added problem of needing to be cleared between runs
    protected void parseVariables() throws InvalidDataException {
        if (!this.isTested) {
            this.test();
        }

        try {
            XMLStreamReader define = XMLInputFactory.newInstance().createXMLStreamReader(
                new FileInputStream(this.define)
            );

            int event, depth = 0;
            boolean inItemGroupDef = false;
            boolean confirmedMetadataOID = false;

            while (define.hasNext()) {
                event = define.next();

                if (event == XMLStreamReader.START_ELEMENT) {
                    String tag = define.getLocalName();

                    if (depth == 0 && !tag.equals("ODM")) {
                        throw new InvalidDataException(
                            InvalidDataException.Codes.CannotParseSource
                        );
                    } else if (depth == 1 && !tag.equals("Study")) {
                        throw new InvalidDataException(
                            InvalidDataException.Codes.CannotParseSource
                        );
                    } else if (depth == 2) {
                        if (tag.equals("MetaDataVersion")) {
                            String oid = define.getAttributeValue(null, "OID");

                            if (!this.metadataOID.equals(oid)) {
                                throw new InvalidDataException(
                                    InvalidDataException.Codes.CannotParseSource
                                );
                            }

                            confirmedMetadataOID = true;
                        } else if (!tag.equals("GlobalVariables")) {
                            throw new InvalidDataException(
                                InvalidDataException.Codes.CannotParseSource
                            );
                        }
                    } else if (depth > 2 && confirmedMetadataOID) {
                        if (depth == 3) {
                            if ( tag.equals("ItemGroupDef")) {
                                String oid = define.getAttributeValue(null, "OID");

                                inItemGroupDef = this.itemgroupOID.equals(oid);
                            } else if (tag.equals("ItemDef")) {
                                String oid = define.getAttributeValue(
                                    null, "OID"
                                );

                                if (oid == null) {
                                    throw new InvalidDataException(
                                        InvalidDataException.Codes.CannotParseSource
                                    );
                                }

                                if (this.oids.containsKey(oid)) {
                                    String name = define.getAttributeValue(
                                        null, "Name"
                                    );

                                    if (name == null) {
                                        throw new InvalidDataException(
                                            InvalidDataException.Codes.CannotParseSource
                                        );
                                    }

                                    this.oids.put(oid, name);
                                }
                            }
                        } else if (depth == 4) {
                            if (inItemGroupDef && tag.equals("ItemRef")) {
                                String oid = define.getAttributeValue(
                                    null, "ItemOID"
                                );

                                if (oid == null) {
                                    throw new InvalidDataException(
                                        InvalidDataException.Codes.CannotParseSource
                                    );
                                }

                                this.oids.put(oid, null);
                            }
                        }
                    }

                    ++depth;
                } else if (depth > 2 && event == XMLStreamReader.END_ELEMENT) {
                    --depth;

                    if (depth == 3 && inItemGroupDef) {
                        inItemGroupDef = false;
                    }
                }
            }

            define.close();

            if (!confirmedMetadataOID) {
                throw new InvalidDataException(
                    InvalidDataException.Codes.CannotParseSource
                );
            }
        } catch (XMLStreamException | IOException ex) {
            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }

        for (Map.Entry<String, String> mapping : this.oids.entrySet()) {
            if (mapping.getValue() == null) {
                throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource,
                        String.format("No name found for ItemOID %s", mapping.getKey()));
            }

            super.metadata.add(mapping.getValue());
        }
    }

    protected List<DataRecord> parseRecords(int recordCount) throws InvalidDataException {
        List<DataRecord> records = new ArrayList<>(recordCount);

        try {
            // This is where we left off in test()
            int event = this.reader.getEventType(), depth = 0;
            Set<String> variables = super.getVariables();
            ImmutableMap.Builder<String, DataEntry> values = null;

            do {
                if (event == XMLStreamReader.START_ELEMENT) {
                    String tag = this.reader.getLocalName();

                    if (depth == 0) {
                        if (!tag.equals("ItemGroupData")) {
                            throw new InvalidDataException(
                                InvalidDataException.Codes.CannotParseSource
                            );
                        }

                        if (values != null) {
                            Set<String> unpopulated = new HashSet<>(variables);

                            unpopulated.removeAll(values.build().keySet());

                            for (String variable : unpopulated) {
                                values.put(variable, DataEntry.NULL_ENTRY);
                            }

                            records.add(super.newRecord(values.build()));
                        }

                        super.next();
                        values = ImmutableMap.builder();
                    } else if (depth == 1 && tag.equals("ItemData")) {
                        String oid = this.reader.getAttributeValue(
                            null, "ItemOID"
                        );

                        if (!this.oids.containsKey(oid)) {
                            throw new InvalidDataException(
                                InvalidDataException.Codes.CannotParseSource
                            );
                        }

                        String value = this.reader.getAttributeValue(
                            null, "Value"
                        );

                        values.put(this.oids.get(oid), this.factory.create(value));
                    }

                    ++depth;
                } else if (event == XMLStreamReader.END_ELEMENT) {
                    --depth;

                    if (depth == 0) {
                        --recordCount;
                    }
                }
            } while (this.reader.hasNext()
                && (event = this.reader.next()) > -1
                &&  depth >= 0
                && recordCount > 0
            );

            if (values != null) {
                Set<String> unpopulated = new HashSet<>(variables);

                unpopulated.removeAll(values.build().keySet());

                for (String variable : unpopulated) {
                    values.put(variable, DataEntry.NULL_ENTRY);
                }

                records.add(super.newRecord(values.build()));
            }

            if (depth < 0 || event == XMLStreamReader.END_ELEMENT) {
                super.markComplete();
            } else if (!this.reader.hasNext()) {
                throw new InvalidDataException(
                    InvalidDataException.Codes.CannotParseSource
                );
            }
        } catch (XMLStreamException ex) {
            throw new InvalidDataException(InvalidDataException.Codes.CannotParseSource);
        }

        return records;
    }
}

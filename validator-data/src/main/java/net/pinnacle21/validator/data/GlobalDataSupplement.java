/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import com.google.common.collect.ImmutableMap;

import java.util.Set;

/**
 * @author Tim Stone
 */
public class GlobalDataSupplement implements DataSupplement {
    public static final String GLOBAL_VARIABLE_PREFIX = "VAR:";

    private static final String KEY_VARIABLE = "PARMCD";
    private static final String VALUE_VARIABLE = "VAL";

    private final ImmutableMap<String, DataEntry> supplementalValues;

    public GlobalDataSupplement(DataSource originalSource) throws InvalidDataException {
        try (DataSource source = originalSource.replicate()) {
            Set<String> variables = source.getVariables();
            String prefix = source.getName();

            ImmutableMap.Builder<String, DataEntry> values = ImmutableMap.builder();

            if (variables.contains(prefix + KEY_VARIABLE) && variables.contains(prefix + VALUE_VARIABLE)) {
                while (source.hasRecords()) {
                    for (DataRecord record : source.getRecords()) {
                        String variable = GLOBAL_VARIABLE_PREFIX + record.getValue(prefix + KEY_VARIABLE).toString()
                            .toUpperCase()
                            .replaceAll("[^A-Z]+", "");

                        if (!values.build().containsKey(variable)) {
                            values.put(variable, record.getValue(prefix + VALUE_VARIABLE));
                        }
                    }
                }
            }

            this.supplementalValues = values.build();
        }
    }

    public DataRecord augment(DataRecord record) {
        return new SupplementalDataRecord(record, this.supplementalValues, false);
    }
}

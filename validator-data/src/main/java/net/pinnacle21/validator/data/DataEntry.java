/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

/**
 * Acts as a wrapper around the value of a cell in a dataset.
 *
 * @author Tim Stone
 */
public interface DataEntry extends Comparable<DataEntry> {
    DataEntry NULL_ENTRY = new DataEntryFactory.NullDataEntry();

    enum DataType {
        Date,
        DateTime,
        Duration,
        Float,
        Integer,
        Text,
        Time,
        Null;

        public boolean isNumeric() {
            return this == Float || this == Integer;
        }

        public boolean isDate() {
            return this == Date || this == DateTime;
        }

        public int getSortOrder() {
            if (this == Null) {
                return 1;
            }

            if (this.isNumeric()) {
                return 2;
            }

            if (this.isDate()) {
                return 3;
            }

            return 4;
        }
    }

    default int compareToAny(Object o) {
        return this.compareToAny(o, true);
    }

    int compareToAny(Object o, boolean caseSensitive);

    DataType getDataType();
    Object getValue();
    boolean isNumeric();
    boolean isDate();
    boolean hasValue();
}

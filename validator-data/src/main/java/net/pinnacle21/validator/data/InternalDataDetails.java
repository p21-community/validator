/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import net.pinnacle21.validator.api.model.DataDetails;

public class InternalDataDetails implements DataDetails {
    private final int id;
    private final Info info;
    private final String name;
    private String key;

    public InternalDataDetails(String name, Info info) {
        this(-1, name, info);
    }

    public InternalDataDetails(int id, Info info) {
        this(id, null, info);
    }

    public InternalDataDetails(int id, String name, Info info) {
        this.id = id;
        this.info = info;
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public Info getInfo() {
        return this.info;
    }

    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public String getName() {
        return this.name;
    }
}

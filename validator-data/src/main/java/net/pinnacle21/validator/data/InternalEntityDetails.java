/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.api.model.VariableDetails;
import net.pinnacle21.validator.util.PropertySet;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author Tim Stone
 */
public final class InternalEntityDetails extends PropertySet<SourceDetails.Property> implements SourceDetails {
    private static final SourceDetails.Property[] UPDATABLE_PROPERTIES = new SourceDetails.Property[] {
        Property.Filtered,
        Property.Location,
        Property.Records,
        Property.Variables
    };

    private final SourceDetails.Reference reference;
    private final SourceDetails parent;
    private final List<SourceDetails> subentities = new LinkedList<>();
    private final List<VariableDetails> variables = new ArrayList<>();
    private final Map<String, Integer> lookup = new HashMap<>();

    public InternalEntityDetails(SourceDetails.Reference reference, String name, String location) {
        super(SourceDetails.Property.class, UPDATABLE_PROPERTIES);

        if (name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }

        if (location == null) {
            throw new IllegalArgumentException("location cannot be null");
        }

        this.reference = reference;
        this.properties.put(SourceDetails.Property.Name, name.toUpperCase());
        this.properties.put(SourceDetails.Property.Location, location);
        this.parent = null;
    }

    public InternalEntityDetails(SourceDetails.Reference reference, String name, String subname, String location) {
        this(reference, name, location);

        if (StringUtils.isNotEmpty(subname)) {
            this.properties.put(SourceDetails.Property.Subname, subname.toUpperCase());
        }
    }

    public InternalEntityDetails(SourceDetails.Reference reference, Map<SourceDetails.Property, Object> properties,
            InternalEntityDetails parent) {
        super(SourceDetails.Property.class, UPDATABLE_PROPERTIES);

        if (properties == null) {
            throw new IllegalArgumentException("properties map cannot be null");
        }

        if (!properties.containsKey(SourceDetails.Property.Name)) {
            throw new IllegalArgumentException("the Name property must always be "
                    + "specified");
        }

        this.reference = reference;
        this.properties.putAll(properties);
        this.parent = parent;
    }

    public InternalEntityDetails(SourceDetails.Reference reference, InternalEntityDetails template) {
        this(reference, template.properties, null);
    }

    public InternalEntityDetails(SourceDetails.Reference reference, InternalEntityDetails template,
            InternalEntityDetails parent) {
        this(reference, template.properties, parent);
    }

    public SourceDetails getParent() {
        return this.parent;
    }

    public SourceDetails.Reference getReference() {
        return this.reference;
    }

    public boolean hasVariable(String name) {
        return this.getVariable(name) != null;
    }

    public VariableDetails getVariable(String name) {
        Integer position = this.lookup.get(name);

        if (position == null) {
            return null;
        }

        return this.variables.get(position);
    }

    public List<VariableDetails> getVariables() {
        return Collections.unmodifiableList(this.variables);
    }

    public void addVariable(VariableDetails variable) {
        this.lookup.put(
            variable.getString(VariableDetails.Property.Name), this.variables.size()
        );
        this.variables.add(variable);
    }

    public void addSubentity(InternalEntityDetails subentity) {
        this.subentities.add(subentity);
    }

    public List<SourceDetails> getSplitSources() {
        return Collections.unmodifiableList(this.subentities);
    }
}

/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.SourceDetails;

import java.util.Set;

/**
 * @author Tim Stone
 */
public class SupplementalDataRecord extends BaseDataRecord {
    private final DataRecord record;
    private final boolean isTransient;

    public SupplementalDataRecord(DataRecord record, ImmutableMap<String, DataEntry> supplementalValues,
            boolean isTransient) {
        super(supplementalValues);

        this.isTransient = isTransient;
        this.record = record;
    }

    @Override
    public boolean definesVariable(String variable) {
        return this.record.definesVariable(variable) || super.definesVariable(variable);
    }

    @Override
    public DataDetails getDataDetails() {
        return this.record.getDataDetails();
    }

    @Override
    public SourceDetails getSourceDetails() {
        return this.record.getSourceDetails();
    }

    @Override
    public int getID() {
        return this.record.getID();
    }

    @Override
    public DataEntry getValue(String variable) {
        if (this.record.definesVariable(variable)) {
            return this.record.getValue(variable);
        }

        return super.getValue(variable);
    }

    @Override
    public Set<String> getVariables() {
        return Sets.union(this.record.getVariables(), super.getVariables());
    }

    @Override
    public boolean isTransient(String variable) {
        return this.record.definesVariable(variable) ? this.record.isTransient(variable) : this.isTransient;
    }
}

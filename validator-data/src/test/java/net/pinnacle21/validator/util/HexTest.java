/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.util;

import org.junit.Test;

import java.security.MessageDigest;

import static org.junit.Assert.assertEquals;

public class HexTest {
    @Test
    public void toStringFunctionGivesExpectedHex() throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");

        digest.update("test string".getBytes());

        assertEquals("661295C9CBF9D6B2F6428414504A8DEED3020641", Hex.toString(digest.digest()));
    }

    @Test
    public void sha1FunctionGivesExpectedHex() {
        assertEquals("661295C9CBF9D6B2F6428414504A8DEED3020641", Hex.sha1("test string"));
    }
}

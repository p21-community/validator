/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import static org.junit.Assert.*;

import net.pinnacle21.validator.api.model.SourceOptions;
import net.pinnacle21.validator.api.model.ValidationOptions;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tim Stone
 */
public class DelimitedDataSourceTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    @Test
    public void NormalDelimitedDataSource() throws Exception {
        Set<String> variables = new HashSet<>(Arrays.asList("A", "B", "C", "D"));

        DataSource source = new DelimitedDataSource(SourceOptions.builder()
            .withName("normal")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/DelimitedDataSource/normal.csv"))
            .build(), this.factory);

        assertTrue(source.test());
        assertEquals(variables, source.getVariables());

        source.getRecords();

        assertEquals(2, source.getRecordCount());
    }

    @Test
    public void HeaderlessDelimitedDataSourceWithTest() throws Exception {
        Set<String> variables = new HashSet<>(Arrays.asList("V1", "V2", "V3", "V4"));

        DataSource source = new DelimitedDataSource(SourceOptions.builder()
            .withName("headerlress")
            .withProperty("Delimiter", ",")
            .withProperty("Header", "false")
            .withSource(this.pathTo("/samples/data/DelimitedDataSource/headerless.csv"))
            .build(), this.factory);

        assertTrue(source.test());
        assertEquals(variables, source.getVariables());

        source.getRecords();

        assertEquals(2, source.getRecordCount());
    }

    @Test
    public void HeaderlessDelimitedDataSource() throws Exception {
        Set<String> variables = new HashSet<>(Arrays.asList("V1", "V2", "V3", "V4"));

        DataSource source = new DelimitedDataSource(SourceOptions.builder()
            .withName("headerlress")
            .withProperty("Delimiter", ",")
            .withProperty("Header", "false")
            .withSource(this.pathTo("/samples/data/DelimitedDataSource/headerless.csv"))
            .build(), this.factory);

        assertEquals(variables, source.getVariables());

        source.getRecords();

        assertEquals(2, source.getRecordCount());
    }

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }
}

/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import net.pinnacle21.validator.api.model.SourceOptions;
import net.pinnacle21.validator.api.model.ValidationOptions;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tim Stone
 */
public class DatasetXmlDataSourceTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }

    @Test
    public void MetadataIsCorrect() throws Exception {
        Set<String> variables = new HashSet<>(Arrays.asList(
            "STUDYID", "DOMAIN", "ARMCD", "ARM", "TAETORD", "ETCD", "ELEMENT", "TABRANCH", "TATRANS", "EPOCH"
        ));

        DataSource source = new DatasetXmlDataSource(SourceOptions.builder()
            .withName("TA")
            .withSource(this.pathTo("/samples/data/DatasetXmlDataSource/ta.xml"))
            .build(), this.factory);

        assertTrue(source.test());
        assertEquals(variables, source.getVariables());
    }

    @Test
    public void CanReadReferenceDataRecords() throws Exception {
        DataSource source = new DatasetXmlDataSource(SourceOptions.builder()
            .withName("TA")
            .withSource(this.pathTo("/samples/data/DatasetXmlDataSource/ta.xml"))
            .build(), this.factory);

        assertTrue(source.test());
        assertEquals(2, source.getRecords().size());
        assertFalse(source.hasRecords());
    }

    @Test
    public void CanReadSubjectDataRecords() throws Exception {
        DataSource source = new DatasetXmlDataSource(SourceOptions.builder()
            .withName("DM")
            .withSource(this.pathTo("/samples/data/DatasetXmlDataSource/dm.xml"))
            .build(), this.factory);

        assertTrue(source.test());
        assertEquals(10, source.getRecords().size());
        assertEquals(1, source.getRecords().size());
        assertFalse(source.hasRecords());
    }

    @Test
    public void CanReadSubjectDataRecordsFromDefineWithNonsense() throws Exception {
        DataSource source = new DatasetXmlDataSource(SourceOptions.builder()
            .withName("DM")
            .withSource(this.pathTo("/samples/data/DatasetXmlDataSource/dm.xml"))
            .withProperty("Define", this.pathTo("/samples/data/DatasetXmlDataSource/define-with-nonsense.xml"))
            .build(), this.factory);

        assertTrue(source.test());
        assertEquals(10, source.getRecords().size());
        assertEquals(1, source.getRecords().size());
        assertFalse(source.hasRecords());
    }
}

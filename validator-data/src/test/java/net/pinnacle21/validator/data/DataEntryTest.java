/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import static org.junit.Assert.*;

import net.pinnacle21.validator.api.model.ValidationOptions;
import org.junit.Test;

/**
 *
 * @author Tim Stone
 */
public class DataEntryTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    /**
     * Ensures that partial date equality works when one date value is only a year, and
     * the other is the same year plus a month and a day.
     */
    @Test
    public void testLeadingYearPartialDateEqualitySuccess() {
        DataEntry lhs;
        DataEntry rhs;
        Object literal;

        // Confirm leading LHS (string-based) value matches
        lhs = this.factory.create("2011");
        rhs = this.factory.create("2011-11-01");

        assertEquals(0, lhs.compareToAny(rhs));

        // Confirm leading LHS (number-based) value matches
        lhs = this.factory.create(2011);
        rhs = this.factory.create("2011-11-01");

        assertEquals(0, lhs.compareToAny(rhs));

        // Confirm leading RHS value matches
        lhs = this.factory.create("2010-04-13");
        rhs = this.factory.create("2010");

        assertEquals(0, lhs.compareToAny(rhs));

        // Confirm leading RHS (number-based) value matches
        lhs = this.factory.create("2010-04-13");
        rhs = this.factory.create(2010);

        assertEquals(0, lhs.compareToAny(rhs));

        // Confirm leading literal (string-based) value matches
        lhs = this.factory.create("2009-07-21");
        literal = "2009";

        assertEquals(0, lhs.compareToAny(literal));

        // Confirm leading literal (string-based) value matches
        lhs = this.factory.create("2008-09-24");
        literal = 2008;

        assertEquals(0, lhs.compareToAny(literal));
    }

    @Test
    public void VeryLargeTextValuesShouldBeLeftAlone() {
        DataEntry entry = this.factory.create("9007199254740993");

        assertEquals("9007199254740993", entry.toString());
    }

    @Test
    public void WhatGoesInMustComeOut() {
        DataEntry entry = this.factory.create("12.00");

        assertEquals("12.00", entry.toString());
    }

    @Test
    public void UseMeaningfulAccuracy() {
        DataEntry entry = this.factory.create(2.2800000000000002);
        DataEntry expected = this.factory.create("2.28");

        assertEquals(expected, entry);
    }

    @Test
    public void ShouldAcceptMissingLeadingZeroes() {
        DataEntry entry = this.factory.create(".5");
        DataEntry expected = this.factory.create(0.5);

        assertEquals(expected, entry);
    }

    @Test
    public void ShouldAcceptNegativeMissingLeadingZeroes() {
        DataEntry entry = this.factory.create("-.5");
        DataEntry expected = this.factory.create(-0.5);

        assertEquals(expected, entry);
    }

    @Test
    public void RoundWithExpectedDecimalPlacesMatch() {
        DataEntry entry = this.factory.create(1.11112356);
        DataEntry expected = this.factory.create(1.11112354);

        assertEquals(expected, entry);
    }

    @Test
    public void RoundWithExpectedDecimalPlacesDontMatch() {
        DataEntry entry = this.factory.create(1.11112356);
        DataEntry expected = this.factory.create(1.1111233);

        assertNotEquals(expected, entry);
    }

    @Test
    public void PlusSignsAreAllowedBeforeNumbers() {
        DataEntry entry = this.factory.create("+1.0");
        DataEntry expected = this.factory.create(1);

        assertEquals(expected, entry);
    }

    @Test
    public void trailingSpacesAreRemoved() {
        DataEntry entry = this.factory.create("ABC  ");

        assertEquals("ABC", entry.getValue().toString());
    }

    @Test
    public void trailingOtherWhiteSpaceIsLeftAlone() {
        DataEntry entry = this.factory.create("ABC  \n");

        assertEquals("ABC  \n", entry.getValue().toString());
    }

    @Test
    public void leadingSpacesAreLeftAlone() {
        DataEntry entry = this.factory.create("   ABC");

        assertEquals("   ABC", entry.getValue().toString());
    }

    @Test
    public void spaceOnlyStringResultsInNull() {
        DataEntry entry = this.factory.create("   ");

        assertNull(entry.getValue());
    }
}

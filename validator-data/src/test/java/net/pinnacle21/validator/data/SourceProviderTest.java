/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import static org.junit.Assert.*;

import java.util.List;

import net.pinnacle21.validator.api.model.SourceOptions;
import net.pinnacle21.validator.api.model.ValidationOptions;
import org.junit.Test;

/**
 *
 * @author Tim Stone
 */
public class SourceProviderTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    @Test
    public void CreateCombinedSource() throws Exception {
        SourceProvider provider = new SourceProvider(this.factory);

        provider.add(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBCH")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbch.csv"))
            .build());
        provider.add(SourceOptions.builder()
            .withType(SourceOptions.StandardTypes.Delimited)
            .withName("LB")
            .withSubname("LBHE")
            .withProperty("Delimiter", ",")
            .withSource(this.pathTo("/samples/data/CombinedDataSource/lbhe.csv"))
            .build());

        assertTrue(provider.containsSource("LB"));

        DataSource source = provider.getSource("LB");
        List<DataRecord> records = source.getRecords();

        assertTrue(source.isComposite());
        assertEquals(4, records.size());
    }

    private String pathTo(String file) {
        return this.getClass().getResource(file).getFile();
    }
}

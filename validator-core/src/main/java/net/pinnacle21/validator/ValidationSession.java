/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator;

import net.pinnacle21.validator.api.model.CancellationToken;
import net.pinnacle21.validator.data.DataEntryFactory;
import net.pinnacle21.validator.data.LookupProvider;
import net.pinnacle21.validator.data.LookupProviderFactory;
import net.pinnacle21.validator.api.model.ValidationOptions;

import java.util.UUID;

/**
 *
 * @author Tim Stone
 */
public class ValidationSession {
    private final UUID id;
    private final ValidationOptions options;
    private final CancellationToken cancellationToken;
    private final LookupProviderFactory lookupProviderFactory;
    private final DataEntryFactory entryFactory;

    private ValidationSession(UUID id, ValidationOptions options, CancellationToken cancellationToken,
            DataEntryFactory entryFactory, LookupProviderFactory lookupProviderFactory) {
        this.id = id;
        this.options = options;
        this.cancellationToken = cancellationToken;
        this.entryFactory = entryFactory;
        this.lookupProviderFactory = lookupProviderFactory;
    }

    public static ValidationSession create(ValidationOptions options, CancellationToken cancellationToken,
            DataEntryFactory entryFactory, LookupProviderFactory lookupProviderFactory) {
        return new ValidationSession(UUID.randomUUID(), options, cancellationToken, entryFactory, lookupProviderFactory);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ValidationSession)) {
            return false;
        }

        return ((ValidationSession)o).id.equals(this.id);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    public String getID() {
        return this.id.toString();
    }

    public boolean shouldCancel() {
        return this.cancellationToken != null && this.cancellationToken.isCancellationRequested();
    }

    public ValidationOptions getOptions() {
        return this.options;
    }

    public LookupProvider getLookupProvider(String adapter) {
        return this.lookupProviderFactory.getProvider(adapter);
    }

    public DataEntryFactory getDataEntryFactory() {
        return this.entryFactory;
    }
}

/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.report;

import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;

import java.util.*;
import java.util.function.ToLongFunction;

/**
 * @author Tim Stone
 */
public class WritableRuleMetricsImpl implements WritableRuleMetrics {
    private final Map<String, Domain> domains = new HashMap<>();

    public Set<String> getDomains() {
        return Collections.unmodifiableSet(this.domains.keySet());
    }

    public Set<String> getDomainRules(String name) {
        Domain domain = getDomain(name);

        if (domain == null) {
            return Collections.emptySet();
        }

        return Collections.unmodifiableSet(domain.rules.keySet());
    }

    @Override
    public long getExecutionCount() {
        long count = 0;

        for (String domain : this.domains.keySet()) {
            count += this.getDomainExecutionCount(domain);
        }

        return count;
    }

    @Override
    public long getFailureCount() {
        long count = 0;

        for (String domain : this.domains.keySet()) {
            count += this.getDomainFailureCount(domain);
        }

        return count;
    }

    @Override
    public long getInvocationCount() {
        long count = 0;

        for (String domain : this.domains.keySet()) {
            count += this.getDomainInvocationCount(domain);
        }

        return count;
    }

    @Override
    public long getDomainExecutionCount(String domainName) {
        return this.getDomainExecutionCount(domainName, null);
    }

    @Override
    public long getDomainExecutionCount(String domainName, Diagnostic.Type type) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return 0;
        }

        return domain.sumOver(RuleMetric::getExecutions, type);
    }

    @Override
    public long getDomainFailureCount(String domainName) {
        return this.getDomainFailureCount(domainName, null);
    }

    @Override
    public long getDomainFailureCount(String domainName, Diagnostic.Type type) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return 0;
        }

        return domain.sumOver(RuleMetric::getFailures, type);
    }

    @Override
    public long getDomainInvocationCount(String domainName) {
        return this.getDomainInvocationCount(domainName, null);
    }

    @Override
    public long getDomainInvocationCount(String domainName, Diagnostic.Type type) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return 0;
        }

        return domain.sumOver(RuleMetric::getInvocations, type);
    }

    public Map<String, Set<String>> getDomainContexts(String domainName) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return Collections.emptyMap();
        }

        Map<String, Set<String>> contexts = new HashMap<>();

        for (Map.Entry<String, Map<String, RuleMetric>> rule : domain.rules.entrySet()) {
            contexts.put(rule.getKey(),
                Collections.unmodifiableSet(rule.getValue().keySet()));
        }

        return contexts;
    }

    public int getRuleExecutionCount(String domainName, String id, String context) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return 0;
        }

        return domain.getExecutionCount(id, context);
    }

    public int getRuleFailureCount(String domainName, String id, String context) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return 0;
        }

        return domain.getFailureCount(id, context);
    }

    public int getRuleInvocationCount(String domainName, String id, String context) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return 0;
        }

        return domain.getInvocationCount(id, context);
    }

    public long getRuleElapsedCount(String domainName, String id, String context) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return 0;
        }

        return domain.getElapsedTime(id, context);
    }

    public SourceDetails.Reference getRuleReference(String domainName, String id, String context) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return null;
        }

        return domain.getRule(id, context).getReference();
    }

    public String getRuleMessage(String domainName, String id, String context) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return null;
        }

        return domain.getRule(id, context).getMessage();
    }

    public Diagnostic.Type getRuleType(String domainName, String id, String context) {
        Domain domain = getDomain(domainName);

        if (domain == null) {
            return null;
        }

        return domain.getRule(id, context).getType();
    }

    private Domain getDomain(String domainName) {
        return this.domains.get(domainName.toUpperCase());
    }

    public RuleMetric getRule(String domainName, SourceDetails.Reference reference, String id, String context,
            String message, Diagnostic.Type type) {
        Domain domain;

        synchronized(this.domains) {
            domain = getDomain(domainName);

            if (domain == null) {
                this.domains.put(domainName, (domain = new Domain()));
            }
        }

        return domain.getRule(id, context, true, reference, message, type);
    }

    private static class Domain {
        private final Map<String, Map<String, RuleMetric>> rules = new HashMap<>();

        int getExecutionCount(String id, String context) {
            RuleMetric rule = getRule(id, context);

            if (rule == null) {
                return 0;
            }

            return rule.getExecutions();
        }

        int getFailureCount(String id, String context) {
            RuleMetric rule = getRule(id, context);

            if (rule == null) {
                return 0;
            }

            return rule.getFailures();
        }

        int getInvocationCount(String id, String context) {
            RuleMetric rule = getRule(id, context);

            if (rule == null) {
                return 0;
            }

            return rule.getInvocations();
        }

        long getElapsedTime(String id, String context) {
            RuleMetric rule = getRule(id, context);

            if (rule == null) {
                return 0;
            }

            return rule.getElapsed();
        }

        long sumOver(ToLongFunction<RuleMetric> propertyMapper, Diagnostic.Type type) {
            return this.rules.values()
                .stream()
                .flatMap(m -> m.values().stream())
                .filter(r -> type == null || r.getType() == type)
                .mapToLong(propertyMapper)
                .sum();
        }

        RuleMetric getRule(String id, String context) {
            return this.getRule(id, context, false, null, null, null);
        }

        RuleMetric getRule(String id, String context, boolean create, SourceDetails.Reference reference,
                String message, Diagnostic.Type type) {
            Map<String, RuleMetric> instances;

            synchronized (this.rules) {
                instances = this.rules.get(id);

                if (instances == null) {
                    if (create) {
                        this.rules.put(id, (instances = new HashMap<>()));
                    } else {
                        return null;
                    }
                }
            }

            RuleMetric rule = instances.get(context);

            if (rule == null) {
                if (create) {
                    instances.put(context, (rule = new RuleMetricImpl(reference, message, type)));
                } else {
                    return null;
                }
            }

            return rule;
        }
    }

    private static class RuleMetricImpl implements RuleMetric {
        private final SourceDetails.Reference reference;
        private final String message;
        private final Diagnostic.Type type;
        private volatile int invocations = 0;
        private volatile int executions = 0;
        private volatile int failures = 0;
        private volatile long elapsed = 0;
        private volatile long start = 0;

        private RuleMetricImpl(SourceDetails.Reference reference, String message, Diagnostic.Type type) {
            this.reference = reference;
            this.message = message;
            this.type = type;
        }

        public void start() {
            this.start = System.nanoTime();
        }

        // TODO: See if these volatile r/ws are more costly than e.g. LongAdder
        @SuppressWarnings("NonAtomicOperationOnVolatileField")
        public void stop(boolean executed, boolean failed) {
            long finish = System.nanoTime();

            this.invocations++;

            if (executed) {
                this.executions++;

                if (failed) {
                    this.failures++;
                }
            }

            this.elapsed += finish - this.start;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public Diagnostic.Type getType() {
            return this.type;
        }

        @Override
        public SourceDetails.Reference getReference() {
            return this.reference;
        }

        @Override
        public int getInvocations() {
            return this.invocations;
        }

        @Override
        public int getExecutions() {
            return this.executions;
        }

        @Override
        public int getFailures() {
            return this.failures;
        }

        @Override
        public long getElapsed() {
            return this.elapsed;
        }
    }
}

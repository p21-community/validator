/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.report;

import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.RuleMetrics;
import net.pinnacle21.validator.api.model.SourceDetails;

/**
 * @author Tim Stone
 */
public interface WritableRuleMetrics extends RuleMetrics {
    RuleMetric getRule(String name, SourceDetails.Reference reference, String id, String context, String message,
            Diagnostic.Type type);

    class Scope {
        private final WritableRuleMetrics metrics;
        private final String domain;
        private final SourceDetails.Reference reference;

        public Scope(WritableRuleMetrics metrics, String domain, SourceDetails.Reference reference) {
            this.metrics = metrics;
            this.domain = domain;
            this.reference = reference;
        }

        public String getDomain() {
            return this.domain;
        }

        public SourceDetails.Reference getReference() {
            return this.reference;
        }

        public RuleMetric getRule(String id, String context, String message, Diagnostic.Type type) {
            return this.metrics.getRule(this.domain, this.reference, id, context, message, type);
        }
    }

    interface RuleMetric {
        void start();
        void stop(boolean executed, boolean failed);
        String getMessage();
        Diagnostic.Type getType();
        SourceDetails.Reference getReference();
        int getInvocations();
        int getExecutions();
        int getFailures();
        long getElapsed();
    }
}

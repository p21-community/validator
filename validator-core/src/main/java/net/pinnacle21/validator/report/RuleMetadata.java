/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.report;

import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.RuleInstance;
import net.pinnacle21.validator.settings.RuleDefinition;

import java.util.Optional;

public class RuleMetadata implements RuleInstance {
    public final String domain;
    public final String id;
    public final String publisherId;
    public final String context;
    public final String description;
    public final String message;
    public final String category;
    public final Diagnostic.Type type;

    public RuleMetadata(RuleDefinition definition, String domain) {
        this(
            domain,
            definition.getId(),
            definition.getProperty("PublisherID"),
            definition.getContext(),
            definition.getProperty("Category"),
            definition.getProperty("Message"),
            definition.getProperty("Description"),
            definition.getType()
        );
    }

    public RuleMetadata(String id, String publisherId, String category, String message, String description,
            Diagnostic.Type type) {
        this(null, id, publisherId, null, category, message, description, type);
    }

    public RuleMetadata(String domain, String id, String publisherId, String context, String category, String message,
            String description, Diagnostic.Type type) {
        this.domain = domain;
        this.id = id;
        this.publisherId = publisherId;
        this.context = context;
        this.category = category;
        this.message = message;
        this.description = description;
        this.type = type;
    }

    @Override
    public String getDomain() {
        return this.domain;
    }

    @Override
    public String getContext() {
        return this.context;
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getPublisherId() {
        return this.publisherId;
    }

    @Override
    public Diagnostic.Type getType() {
        return this.type;
    }

    @Override
    public String toString() {
        return this.id + Optional.ofNullable(this.context)
            .map(c -> "[" + c + "]")
            .orElse("");
    }
}

/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator;

import net.pinnacle21.validator.api.DataValidator;
import net.pinnacle21.validator.api.Validation;
import net.pinnacle21.validator.api.Validator;
import net.pinnacle21.validator.api.model.ConfigOptions;
import net.pinnacle21.validator.api.model.SourceOptions;
import net.pinnacle21.validator.api.model.ValidationOptions;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

public class ValidatorImpl implements DataValidator {
    @Override
    public Validation prepare(List<SourceOptions> sources, ConfigOptions config, ValidationOptions options,
            ExecutorService sharedExecutor) {
        return new ValidationImpl(sources, config, options, sharedExecutor);
    }

    @Override
    public boolean supportsFeature(String feature) {
        return false;
    }
}

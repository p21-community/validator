/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;

import net.pinnacle21.validator.api.events.util.Dispatcher;
import net.pinnacle21.validator.api.model.SourceOptions;
import net.pinnacle21.validator.api.events.ValidationEvent;
import net.pinnacle21.validator.util.ConcurrentCacheMap;
import net.pinnacle21.validator.util.KeyMap;
import org.slf4j.LoggerFactory;

import static net.pinnacle21.validator.util.ValidationEvents.*;

/**
 *
 * @author Tim Stone
 */
public class ConcurrentLookupProvider implements LookupProvider {
    private static final Dispatcher DISPATCHER = new Dispatcher(c -> LoggerFactory.getLogger(c)::error);

    private final Map<String, Map<String, Integer>> requests = new KeyMap<>();
    private final ConcurrentMap<String, DataCache> caches = new ConcurrentCacheMap<>();
    private final Set<Consumer<ValidationEvent>> eventListeners;
    private final SourceProvider provider;
    private final DataEntryFactory factory;

    public ConcurrentLookupProvider(DataEntryFactory factory, SourceProvider provider,
            Set<Consumer<ValidationEvent>> eventListeners) {
        this.factory = factory;
        this.provider = provider;
        this.eventListeners = eventListeners;
    }

    public Lookup get(String sourceName, Set<String> variables) {
        while (true) {
            DataCache cache = this.caches.get(sourceName);

            if (cache != null) {
                if (cache.contains(variables)) {
                    // The cache contains all of the variables we need
                    //noinspection SynchronizationOnLocalVariableOrMethodParameter
                    synchronized (cache) {
                        if (!cache.isComplete()) {
                            try {
                                cache.wait();
                            } catch (InterruptedException ex) {
                                throw new RuntimeException(ex);
                            }
                        }

                        if (!cache.isCorrupt()) {
                            return cache;
                        } else {
                            return null;
                        }
                    }
                } else if (!this.verifyExists(sourceName, variables)) {
                    // The cache can't be created
                    return null;
                }
            }

            boolean isDatasetName = sourceName.matches("[A-Za-z][A-Za-z0-9]*");

            if (cache == null) {
                // The cache doesn't exist at all
                if (isDatasetName && !this.verifyExists(sourceName, variables)) {
                    // This is a dataset and we don't have it or it lacks some variables, so we can't create the cache
                    return null;
                }

                if (this.provider.containsSource(sourceName) && !this.provider.containsValidSource(sourceName)) {
                    // We have the source but we can't generate the cache since the source is corrupt
                    return null;
                }

                cache = new DataCache(this.factory, variables);

                if (this.caches.putIfAbsent(sourceName, cache) == null) {
                    // This will be the thread responsible for filling the cache
                    //noinspection SynchronizationOnLocalVariableOrMethodParameter
                    synchronized (cache) {
                        // Determine if the cache can be made
                        if (!this.provider.containsSource(sourceName)) {
                            SourceOptions options = this.provider.parseSource(sourceName);

                            if (options != null) {
                                this.provider.add(options);
                            }
                        }

                        if (!this.provider.containsValidSource(sourceName)) {
                            // Mark cache as corrupted
                            cache.setCorrupt();
                            // Remove the cache
                            this.caches.remove(sourceName, cache);
                            // Mark the cache as complete
                            cache.setComplete();
                            // Notify all waiters
                            cache.notifyAll();
                            // The source still doesn't exist
                            return null;
                        }

                        try (DataSource source = this.provider.getSource(sourceName, true)) {
                            if (!source.getVariables().containsAll(variables)) {
                                // Mark cache as corrupted
                                cache.setCorrupt();
                                // Remove the cache
                                this.caches.remove(sourceName, cache);
                                // The source won't work to generate a cache from
                                return null;
                            }

                            while (source.hasRecords()) {
                                cache.store(source.getRecords());

                                DISPATCHER.dispatchTo(this.eventListeners, subprocessingIncrementEvent(source.getName(),
                                        source.getRecordCount()));
                            }

                            return cache;
                        } catch (InvalidDataException ex) {
                            // Mark cache as corrupted
                            cache.setCorrupt();
                            // Remove the cache
                            this.caches.remove(sourceName, cache);
                            // The source won't work to generate a cache from
                            return null;
                        } finally {
                            // Mark the cache as complete
                            cache.setComplete();
                            // Notify all waiters
                            cache.notifyAll();
                        }
                    }
                } else {
                    // Someone's added a cache while we were doing this, so just repeat
                    // the whole process
                    continue;
                }
            } else {
                // The cache needs to be partially regenerated
                if (!this.verifyExists(sourceName, variables)) {
                    // We already have a cache, so we know this is an obtainable source,
                    // but does it have the right variables?
                    return null;
                }

                //noinspection SynchronizationOnLocalVariableOrMethodParameter
                synchronized (cache) {
                    DataCache current = this.caches.get(sourceName);

                    if (current != cache && current.contains(variables)) {
                        // The cache has already been updated by a previous thread, so
                        // just use this
                        return current;
                    }

                    DataCache updated = new DataCache(this.factory, variables, cache);

                    try (DataSource source = this.provider.getSource(sourceName, true)) {
                        source.getVariables();

                        while (source.hasRecords()) {
                            updated.store(source.getRecords());

                            DISPATCHER.dispatchTo(this.eventListeners, subprocessingIncrementEvent(source.getName(),
                                    source.getRecordCount()));
                        }

                        // Replace the existing cache
                        this.caches.replace(sourceName, cache, updated);

                        return updated;
                    } catch (InvalidDataException ex) {
                        // The source won't work to generate a cache from
                        return null;
                    } finally {
                        // Mark the cache as complete
                        updated.setComplete();
                    }
                }
            }
        }
    }

    public synchronized void request(String sourceName, Set<String> variables) {
        Map<String, Integer> requestedVariables;

        if (this.caches.containsKey(sourceName)) {
            requestedVariables = this.requests.get(sourceName);
        } else {
            requestedVariables = new KeyMap<>();
        }

        for (String variable : variables) {
            if (requestedVariables.containsKey(variable)) {
                requestedVariables.put(variable, requestedVariables.get(variable) + 1);
            } else {
                requestedVariables.put(variable, 1);
            }
        }

        this.requests.put(sourceName, requestedVariables);
    }

    public boolean verifyExists(String sourceName) {
        return this.provider.containsSource(sourceName);
    }

    public boolean verifyExists(String sourceName, Set<String> variables) {
        boolean result = false;

        if (this.verifyExists(sourceName)) {
            DataSource source = this.provider.getSource(sourceName);

            try {
                result = source.getVariables().containsAll(variables);
            } catch (InvalidDataException ex) {
                result = false;
            }
        }

        return result;
    }
}

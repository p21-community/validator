/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.data;

import java.util.List;
import java.util.Set;

import net.pinnacle21.validator.rules.expressions.PreparedQuery;

/**
 *
 * @author Tim Stone
 */
public interface Lookup {
    /**
     *
     * @param variable
     *
     * @return
     */
    public boolean contains(String variable);

    /**
     *
     * @param variables
     *
     * @return
     */
    public boolean contains(Set<String> variables);

    /**
     *
     * @param variables
     */
    public void release(Set<String> variables);

    /**
     *
     * @param values
     * @param where
     * @param ignoreWhereFailure
     *
     * @return
     */
    public boolean seek(List<PreparedQuery.Mapping> search,
            List<PreparedQuery.Mapping> where, boolean ignoreWhereFailure);
}

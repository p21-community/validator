/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.engine;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;

import net.pinnacle21.validator.ValidationSession;
import net.pinnacle21.validator.api.events.DiagnosticListener;
import net.pinnacle21.validator.api.events.util.Dispatcher;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.api.events.ValidationEvent;
import net.pinnacle21.validator.data.*;
import net.pinnacle21.validator.rules.CorruptRuleException;
import net.pinnacle21.validator.rules.ValidationRule;
import net.pinnacle21.validator.settings.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import static net.pinnacle21.validator.util.ValidationEvents.*;

/**
 * @author Tim Stone
 */
public class BlockValidator implements ValidationEngine {
    private static final Logger LOGGER = LoggerFactory.getLogger(BlockValidator.class);
    private static final Dispatcher DISPATCHER = new Dispatcher(c -> LoggerFactory.getLogger(c)::error);

    private final Set<EngineEntity> entities = new LinkedHashSet<>();
    private final Set<DiagnosticListener> diagnosticListeners;
    private final Set<Consumer<ValidationEvent>> eventListeners;
    private final ValidationSession session;
    private final ExecutorService executor;
    private final Configuration global;
    private final List<DataSupplement> supplements = new ArrayList<>();

    public BlockValidator(ValidationSession session, Set<DiagnosticListener> diagnosticListeners,
            Set<Consumer<ValidationEvent>> eventListeners, ExecutorService executor, Configuration global) {
        this.session = session;
        this.diagnosticListeners = diagnosticListeners;
        this.eventListeners = eventListeners;
        this.executor = executor;

        // TODO: This is kind of hacky, specifying the global configuration like this..
        this.global = global;
    }

    public void prepare(EngineEntity entity) {
        this.entities.add(entity);

        SourceDetails details = entity.getDetails();
        String name = details.getString(SourceDetails.Property.Name);

        // TODO: This is a very deliberate hack to get the basic functionality in place
        //    This should absolutely be externalized somehow to the configuration
        try {
            if (name.equals("DM")) {
                this.supplements.add(new SubjectDataSupplement(entity.getSource()));
            } else if (name.equals("TS")) {
                this.supplements.add(new GlobalDataSupplement(entity.getSource()));
            }
        } catch (InvalidDataException ex) {
            throw new RuntimeException(ex);
        }
    }

    public boolean validate() {
        LOGGER.info("Beginning validation tasks");

        List<Callable<Boolean>> tasks = new LinkedList<>();
        CombinedDataSource global = new CombinedDataSource("GLOBAL", "System", this.session.getDataEntryFactory());

        // Set the total validation item count
        //this.metrics.setTotalItems(this.entities.size());
        int itemCount = 0;

        for (EngineEntity entity : this.entities) {
            ++itemCount;

            // Append a task for the data
            tasks.add(new BlockTask(entity, itemCount));

            // TODO: We're avoiding adding the metadata here if the source is corrupt, do we always want that?
            if (!entity.getDetails().getBoolean(SourceDetails.Property.Corrupted, false)) {
                // Prepend a task for the metadata
                tasks.add(0, new BlockTask(new EngineEntity(
                    entity.getSource().getMetadata(),
                    entity.getConfiguration()
                )));

                // Add the metadata to the global data source
                try {
                    global.add(entity.getSource().getMetadata().replicate());
                } catch (InvalidDataException ex) {
                    // Won't happen, because metadata sources don't throw this exception
                }
            }
        }

        // Check if there's anything to process
        if (!tasks.isEmpty()) {
            tasks.add(0, new BlockTask(new EngineEntity(global, this.global)));

            try {
                for (Future<Boolean> taskOutcome : this.executor.invokeAll(tasks)) {
                    boolean result = false;

                    try {
                        result = taskOutcome.get();
                    } catch (ExecutionException ex) {
                        LOGGER.error("Unable to determine validation task result because the task threw an exception", ex);
                    }

                    if (!result) {
                        return false;
                    }
                }
            } catch (InterruptedException ex) {
                LOGGER.warn("Validation was interrupted");
            } finally {
                LOGGER.info("Finished validation tasks");
            }
        }

        return true;
    }

    private class BlockTask implements Callable<Boolean> {
        private final int id;
        private final EngineEntity entity;

        BlockTask(EngineEntity entity) {
            this(entity, 0);
        }

        BlockTask(EngineEntity entity, int taskNumber) {
            this.id = taskNumber;
            this.entity = entity;
        }

        public Boolean call() {
            InternalEntityDetails details = this.entity.getDetails();
            DataSource source = this.entity.getSource();
            String sourceName = details.getString(SourceDetails.Property.Name);
            boolean isGlobal = sourceName.equalsIgnoreCase("GLOBAL");

            MDC.put("dataset_name", sourceName);
            MDC.put("dataset_is_metadata",
                    Boolean.toString(details.getReference() == SourceDetails.Reference.Metadata));

            LOGGER.info("Beginning validation of dataset");

            DISPATCHER.dispatchTo(eventListeners, processingStartEvent(sourceName, this.id, entities.size()));

            boolean completedSuccessfully = false;

            try {
                Set<ValidationRule> filters = this.entity.getFilters();
                Set<ValidationRule> rules = this.entity.getRules();

                LOGGER.debug("Setting up filters");

                // Perform setup activities on all filters/rules
                for (ValidationRule filter : filters) {
                    filter.setup(details);
                }

                LOGGER.debug("Setting up rules");

                for (ValidationRule rule : rules) {
                    rule.setup(details);
                }

                this.validateRecords(source, details, isGlobal, filters, rules);
                this.validateDataset(details, rules);

                details.setProperty(SourceDetails.Property.Validated, true);

                completedSuccessfully = true;
            } catch (InterruptedException ex) {
                throw new CancellationException();
            } catch (Exception ex) {
                LOGGER.error("Unexpected exception while performing validation", ex);
            } finally {
                DISPATCHER.dispatchTo(eventListeners, processingStopEvent(sourceName, this.id, entities.size()));

                LOGGER.info("Finished validation of dataset");
            }

            return completedSuccessfully;
        }

        private void validateRecords(DataSource source, InternalEntityDetails details, boolean isGlobal,
                Set<ValidationRule> filters, Set<ValidationRule> rules) throws InterruptedException {
            int totalValidatedRecords = 0;
            int totalExaminedRecords = 0;
            boolean isFiltered = false;

            LOGGER.debug("Beginning record-level validations");

            try {
                // Process all of the records in the dataset
                while (source.hasRecords()) {
                    List<Diagnostic> diagnostics = new ArrayList<>();

                    // Get the next batch of records
                    for (DataRecord record : source.getRecords()) {
                        ///////////////////////
                        //    Abort Point    //
                        ///////////////////////
                        this.isAlive();

                        ++totalExaminedRecords;

                        LOGGER.trace("Examining record {}", totalExaminedRecords);

                        if (!source.isMetadata() && !isGlobal) {
                            for (DataSupplement supplement : supplements) {
                                record = supplement.augment(record);
                            }
                        }

                        if (!filters.isEmpty()) {
                            Iterator<ValidationRule> filterIterator = filters.iterator();
                            boolean filtered = false;

                            while (!filtered && filterIterator.hasNext()) {
                                ///////////////////////
                                //    Abort Point    //
                                ///////////////////////
                                this.isAlive();

                                ValidationRule filter = filterIterator.next();

                                try {
                                    filtered = !filter.validate(record);
                                } catch (CorruptRuleException ex) {
                                    if (ex.getState() == CorruptRuleException.State.Unrecoverable) {
                                        filterIterator.remove();
                                    }
                                }
                            }

                            if (filtered) {
                                isFiltered = true;

                                LOGGER.trace("Excluding record {} due to filters", totalExaminedRecords);

                                continue;
                            }
                        }

                        Iterator<ValidationRule> ruleIterator = rules.iterator();

                        while (ruleIterator.hasNext()) {
                            ///////////////////////
                            //    Abort Point    //
                            ///////////////////////
                            this.isAlive();

                            ValidationRule rule = ruleIterator.next();

                            try {
                                LOGGER.trace("Attempting validation of record {} against rule {}",
                                        totalExaminedRecords, rule.getMetadata());

                                // Perform the validation against this record
                                rule.validate(record, diagnostics::add);
                            } catch (CorruptRuleException ex) {
                                CorruptRuleException.State corruptionState = ex.getState();

                                LOGGER.debug("Validation of record {} with rule {} resulted in an exception",
                                    totalExaminedRecords, rule.getMetadata(), ex);

                                if (corruptionState == CorruptRuleException.State.Unrecoverable) {
                                    LOGGER.debug("Rule {} has been removed after throwing an exception on record {}",
                                        rule.getMetadata(), totalExaminedRecords);

                                    // Remove the rule, since we can't use it anymore
                                    ruleIterator.remove();
                                }
                            }
                        }

                        ++totalValidatedRecords;
                    }

                    DISPATCHER.dispatchTo(diagnosticListeners, d -> d, diagnostics);
                    DISPATCHER.dispatchTo(eventListeners, processingIncrementEvent(source.getName(), this.id,
                            entities.size(), totalExaminedRecords));
                }
            } catch (InvalidDataException ex) {
                LOGGER.warn("Unable to fully complete validation of dataset due to an exception while reading", ex);
            } finally {
                LOGGER.debug("Finished record-level validations, {} records validated out of {} examined",
                        totalValidatedRecords, totalExaminedRecords);

                details.setProperty(SourceDetails.Property.Filtered, isFiltered);

                DISPATCHER.dispatchTo(diagnosticListeners, d -> d::complete , details);
            }
        }

        private void validateDataset(InternalEntityDetails details, Set<ValidationRule> rules)
                throws InterruptedException {
            List<Diagnostic> diagnostics = new ArrayList<>();

            LOGGER.debug("Beginning dataset-level validation");

            try {
                for (ValidationRule rule : rules) {
                    ///////////////////////
                    //    Abort Point    //
                    ///////////////////////
                    this.isAlive();

                    LOGGER.trace("Attempting validation of dataset against rule {}", rule.getMetadata());

                    rule.validateDataset(details, diagnostics::add);
                }
            } finally {
                LOGGER.debug("Finished dataset-level validation");
            }

            DISPATCHER.dispatchTo(diagnosticListeners, d -> d, diagnostics);
        }

        private void isAlive() throws InterruptedException {
            if (Thread.currentThread().isInterrupted() || session.shouldCancel()) {
                throw new InterruptedException();
            }
        }
    }
}

/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.engine;

import java.util.Collections;
import java.util.Set;

import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.data.DataSource;
import net.pinnacle21.validator.data.InternalEntityDetails;
import net.pinnacle21.validator.rules.ValidationRule;
import net.pinnacle21.validator.settings.Configuration;

/**
 *
 *
 * @author Tim Stone
 */
public class EngineEntity {
    private final Configuration configuration;
    private final DataSource source;
    private final SourceDetails.Reference reference;

    public EngineEntity(DataSource source, Configuration configuration) {
        this.source = source;
        this.configuration = configuration;

        if (this.configuration != null) {
            InternalEntityDetails details = this.source.getDetails();

            details.setProperty(SourceDetails.Property.Keys, configuration.getProperty("DomainKeys"));
            details.setProperty(SourceDetails.Property.Class, configuration.getProperty("Class"));
            details.setProperty(SourceDetails.Property.Configuration, configuration.getProperty("Configuration"));

            if (!details.hasProperty(SourceDetails.Property.Label)) {
                details.setProperty(SourceDetails.Property.Label, configuration.getProperty("Label"));
            }

            this.reference = details.getReference();
        } else {
            this.reference = null;
        }
    }

    public Configuration getConfiguration() {
        return this.configuration;
    }

    public InternalEntityDetails getDetails() {
        return this.source.getDetails();
    }

    public Set<ValidationRule> getFilters() {
        if (this.configuration == null || this.reference == SourceDetails.Reference.Metadata) {
            return Collections.emptySet();
        }

        return this.configuration.getFilters();
    }

    public Set<ValidationRule> getRules() {
        if (this.configuration == null) {
            return Collections.emptySet();
        }

        return this.configuration.getRules(this.reference);
    }

    public DataSource getSource() {
        return this.source;
    }
}

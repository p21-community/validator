/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.util;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author Tim Stone
 */
public class Helpers {
    public static String[] trimSplit(String string, String delimiter) {
        String[] split = string.split(Pattern.quote(delimiter));

        for (int i = 0; i < split.length; ++i) {
            split[i] = split[i].trim();
        }

        return split;
    }

    public static int[] determineLargestIncreasingSubsequence(int[] input) {
        return determineLargestIncreasingSubsequence(input, false);
    }

    public static int[] determineLargestIncreasingSubsequence(int[] input, boolean returnIndexes) {
        int length = 0, count = input.length + 1;
        int[] current  = new int[count];
        int[] previous = new int[count];

        System.arraycopy(input, 0, current, 1, input.length);

        input = current;
        current = new int[count];

        for (int i = 1, j = 0; i < count; ++i) {
            if (length == 0 || input[current[1]] >= input[i]) {
                j = 0;
            } else {
                int low = 1, high = length + 1, mid;

                while (low < high - 1) {
                    mid = (low + high) / 2;

                    if (input[current[mid]] < input[i]) {
                        low = mid;
                    } else {
                        high = mid;
                    }
                }

                j = low;
            }

            previous[i] = current[j];

            if (j == length || input[i] < input[current[j + 1]]) {
                current[j + 1] = i;
                length = Math.max(length, j + 1);
            }
        }

        int[] subsequence = new int[length];
        int position = current[length];

        while (length > 0) {
            subsequence[length - 1] = returnIndexes? position - 1 : input[position];
            position = previous[position];

            --length;
        }

        return subsequence;
    }

    public static <I> List<Class<I>> find(Class<I> definition) {
        return find(definition, true);
    }

    @SuppressWarnings("unchecked")
    public static <I> List<Class<I>> find(Class<I> definition, boolean useCache) {
        List<Class<I>> implementations = new ArrayList<Class<I>>();

        try {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Enumeration<URL> resources = loader.getResources(
                "META-INF/services/" + definition.getName()
            );

            while (resources.hasMoreElements()) {
                URL url = resources.nextElement();
                InputStream stream = url.openStream();

                try {
                    BufferedReader reader = new BufferedReader(
                        new InputStreamReader(stream, "UTF-8")
                    );

                    String line;

                    while ((line = reader.readLine()) != null) {
                        int comment = line.indexOf('#');

                        if (comment == 0) {
                            continue;
                        }

                        if (comment > 0) {
                            line = line.substring(0, comment);
                        }

                        line = line.trim();

                        if (line.length() == 0) {
                            continue;
                        }

                        try {
                            Class<?> implementation = Class.forName(line, true, loader);

                            if (definition.isAssignableFrom(implementation)) {
                                implementations.add((Class<I>)implementation);
                            }
                        } catch (ClassNotFoundException ignore) {}
                    }
                } finally {
                    stream.close();
                }
            }
        } catch (IOException ignore) {}

        return implementations;
    }
}

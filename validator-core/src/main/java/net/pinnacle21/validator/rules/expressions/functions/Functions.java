/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import java.lang.reflect.Constructor;
import java.util.*;

import net.pinnacle21.validator.data.DataEntryFactory;
import net.pinnacle21.validator.rules.expressions.SyntaxException;

/**
 *
 * @author Tim Stone
 */
public class Functions {
    private static Map<String, Class<? extends Function>> functions = new HashMap<>();

    static {
        functions.put("DATE", SasDateTime.class);
        functions.put("TIME", SasDateTime.class);
        functions.put("MAX", Maximum.class);
        functions.put("DIV", Division.class);
        functions.put("DY", DyCount.class);
        functions.put("DIFF", Difference.class);
        functions.put("PCTDIFF", PercentDifference.class);
        functions.put("DYADD", DyAdd.class);
    }

    public static Function create(String function, DataEntryFactory factory) {
        if (!function.startsWith(":")) {
            throw new SyntaxException(String.format("'%s' isn't a function", function));
        }

        // Remove the colon
        function = function.substring(1);

        // Now get the name
        int start = function.indexOf('(');

        if (start == -1 || !function.endsWith(")")) {
            throw new SyntaxException(String.format(
                "'%s' doesn't have the correct parentheses", function
            ));
        }

        String name = function.substring(0, start);
        String args = function.substring(start + 1, function.length() - 1);

        Class<? extends Function> definition = functions.get(name);

        if (definition == null) {
            throw new SyntaxException(String.format("Unknown function '%s'", name));
        }

        try {
            Constructor<? extends Function> constructor = definition.getDeclaredConstructor(
                String.class, DataEntryFactory.class, String[].class
            );

            return constructor.newInstance(name, factory, args.split(","));
        } catch (Exception ex) {
            Throwable cause = ex.getCause();

            if (cause instanceof SyntaxException) {
                throw (SyntaxException)cause;
            }

            throw new RuntimeException(String.format(
                "Unable to create function %s", name
            ), ex);
        }
    }

    private Functions() {}
}

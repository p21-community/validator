/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.api.model.SourceDetails;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author Tim Stone
 */
public class FindValidationRule extends AbstractScriptableValidationRule {
    private static final String DEFAULT_DELIMITER = ",";
    private static final String[] REQUIRED_VARIABLES = new String[] { "Variable" };

    private Set<String> terms = new HashSet<>();
    private boolean isCaseSensitive = true;
    private final int matchCount;
    private final int ifStatements;
    private final boolean matchExact;
    private int counter = 0;
    private final String variable;
    private final String value;
    private final Pattern test;
    private final String[] groups;
    private final Map<DataGrouping, DataGrouping> groupings = new HashMap<>();

    public FindValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Dataset, REQUIRED_VARIABLES, metrics);

        this.variable = definition.getProperty("Variable").toUpperCase();

        super.addVariable(this.variable);

        if (definition.hasProperty("Terms") == definition.hasProperty("Test")) {
            throw new ConfigurationException(
                ConfigurationException.Type.RuleDefinition,
                "Find rules must have one of (Terms, Test), and cannot have both."
            );
        }

        String storeValue = "";
        String delimiter = DEFAULT_DELIMITER;
        int matchCount = -1;

        if (definition.hasProperty("Match")) {
            String value = definition.getProperty("Match");

            if (value.equalsIgnoreCase("One")) {
                matchCount = 1;
            } else {
                try {
                    matchCount = Integer.parseInt(value);
                } catch (NumberFormatException ex) {
                    throw new ConfigurationException(
                        ConfigurationException.Type.RuleDefinition,
                        "Invalid value for Match attribute, expected a number or 'one'",
                        ex
                    );
                }
            }
        }

        this.matchExact = definition.getProperty("MatchExact").equalsIgnoreCase("Yes");

        if (definition.hasProperty("CaseSensitive")) {
            if (definition.getProperty("CaseSensitive").equalsIgnoreCase("No")) {
                this.isCaseSensitive = false;
            }
        }

        if (definition.hasProperty("Terms")) {
            storeValue = definition.getProperty("Terms");

            if (definition.hasProperty("Delimiter")) {
                delimiter = definition.getProperty("Delimiter");
            }

            String[] matchValues = definition.getProperty("Terms").split(
                Pattern.quote(delimiter)
            );

            for (String value : matchValues) {
                if (!this.isCaseSensitive) {
                    value = value.toUpperCase();
                }

                this.terms.add(value.trim());
            }

            this.counter = this.terms.size();

            if (matchCount < 0) {
                matchCount = this.counter;
            }

            this.test = null;
        } else {
            try {
                int mask = this.isCaseSensitive ? 0 : Pattern.CASE_INSENSITIVE;

                this.test = Pattern.compile(definition.getProperty("Test"), mask);
            } catch (PatternSyntaxException ex) {
                throw new ConfigurationException(
                    ConfigurationException.Type.RuleDefinition,
                    "Value for Test attribute is an invalid regular expression",
                    ex
                );
            }

            if (matchCount < 0) {
                matchCount = 1;
            }
        }

        if (definition.hasProperty("If")) {
            String[] tests = definition.getProperty("If").split("@iand");

            for (int i = 0; i < tests.length; ++i) {
                super.prepareExpression("IF" + i, tests[i]);
            }

            this.ifStatements = tests.length;
        } else {
            this.ifStatements = 0;
        }

        if (definition.hasProperty("GroupBy")) {
            String[] groups = definition.getProperty("GroupBy").split(",");
            this.groups = new String[groups.length];

            for (int i = 0; i < groups.length; ++i) {
                this.groups[i] = groups[i].trim().toUpperCase();

                super.addVariable(this.groups[i]);
            }
        } else {
            this.groups = new String[0];

            DataGrouping group = new DataGrouping(
                new DataEntry[0], this.counter, this.ifStatements, this.terms
            );

            this.groupings.put(group, group);
        }

        this.matchCount = matchCount;

        if (definition.hasProperty("When")) {
            super.prepareExpression(definition.getProperty("When"));
        }

        this.value = storeValue;
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        int count = this.groups.length;
        DataEntry[] group = new DataEntry[count];

        for (int i = 0; i < count; ++i) {
            group[i] = dataRecord.getValue(this.groups[i]);
        }

        DataGrouping search = new DataGrouping(
            group, this.counter, this.ifStatements, this.terms
        );
        DataGrouping result = this.groupings.get(search);

        if (result == null) {
            this.groupings.put(search, result = search);
        }

        if (super.hasExpression("IF0") && !result.isActivated()) {
            for (int i = 0; i < this.ifStatements; ++i) {
                if (!result.isActivated(i) && super.checkExpression(dataRecord, "IF" + i)) {
                    result.activate(i);
                }
            }
        }

        DataEntry entry = dataRecord.getValue(this.variable);

        if (super.checkExpression(dataRecord) && entry.hasValue()) {
            String value = entry.toString();

            if (this.test == null) {
                if (!this.isCaseSensitive) {
                    value = value.toUpperCase();
                }

                result.terms.remove(value);
            } else {
                if (this.test.matcher(value).matches()) {
                    ++result.counter;
                }
            }
        }

        return -1;
    }

    protected List<Outcome> performDatasetValidation(SourceDetails entity) {
        List<Outcome> results = new ArrayList<>();

        for (DataGrouping grouping : this.groupings.keySet()) {
            if (!super.hasExpression("IF0") || grouping.isActivated()) {
                boolean result = true;

                if (this.test == null) {
                    if (!this.matchExact) {
                        if (grouping.counter - this.matchCount < grouping.terms.size()) {
                            result = false;
                        }
                    } else {
                        if (grouping.counter - this.matchCount != grouping.terms.size()) {
                            result = false;
                        }
                    }
                } else {
                    if (!this.matchExact) {
                        if (grouping.counter < this.matchCount) {
                            result = false;
                        }
                    } else {
                        if (grouping.counter != this.matchCount) {
                            result = false;
                        }
                    }
                }

                Outcome outcome = new Outcome((byte)(result ? 1 : 0));

                outcome.display.put(this.variable, this.value);

                for (int i = 0; i < this.groups.length; ++i) {
                    outcome.display.put(this.groups[i], grouping.group[i].toString());
                }

                results.add(outcome);
            }
        }

        if (results.isEmpty()) {
            return super.performDatasetValidation(entity);
        }

        return results;
    }

    private static class DataGrouping {
        private final DataEntry[] group;
        private final int hashCode;
        private final boolean[] activated;
        private boolean isActivated;
        final Set<String> terms;
        int counter;

        DataGrouping(DataEntry[] group, int counter, int conditions, Set<String> terms) {
            this.group = group;
            this.activated = new boolean[conditions];

            HashCodeBuilder builder = new HashCodeBuilder(15, 97);

            for (DataEntry entry : group) {
                builder.append(entry);
            }

            this.hashCode = builder.toHashCode();
            this.counter = counter;
            this.terms = new HashSet<>(terms);
        }

        @Override
        public int hashCode() {
            return this.hashCode;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof DataGrouping)) {
                return false;
            }

            if (o == this) {
                return true;
            }

            return new EqualsBuilder().append(
                this.group, ((DataGrouping)o).group
            ).isEquals();
        }

        void activate(int i) {
            this.activated[i] = true;

            for (boolean isActive : this.activated) {
                if (!isActive) {
                    return;
                }
            }

            this.isActivated = true;
        }

        boolean isActivated() {
            return this.isActivated;
        }

        boolean isActivated(int i) {
            return this.activated[i];
        }
    }
}

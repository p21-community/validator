/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

/**
 * @author Tim Stone
 */
public class ConditionalRequiredValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {"Variable"};

    private final String variable;

    public ConditionalRequiredValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Record, REQUIRED_VARIABLES, metrics);

        if (definition.hasProperty("When")) {
            super.prepareExpression(definition.getProperty("When"));
        }

        this.variable = definition.getProperty("Variable").toUpperCase();

        super.addVariable(this.variable);
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        DataEntry entry = dataRecord.getValue(this.variable);

        return (byte)(entry.hasValue() ? 1 : 0);
    }
}

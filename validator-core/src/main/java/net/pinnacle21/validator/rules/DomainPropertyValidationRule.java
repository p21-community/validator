/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.data.DataRecordImpl;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tim Stone
 */
public class DomainPropertyValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "Properties", "Test"
    };

    private final List<SourceDetails.Property> properties = new LinkedList<>();

    public DomainPropertyValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Dataset, REQUIRED_VARIABLES, metrics);
        super.prepareExpression(definition.getProperty("Test"), false);

        for (String property : definition.getProperty("Properties").split(",")) {
            this.properties.add(SourceDetails.Property.valueOf(property.trim()));
        }
    }

    protected List<Outcome> performDatasetValidation(SourceDetails entity) {
        List<Outcome> results = new LinkedList<>();
        List<SourceDetails> entities = entity.hasProperty(SourceDetails.Property.Combined)
            ?  entity.getSplitSources()
            : Collections.singletonList(entity);

        for (SourceDetails dataset : entities) {
            ImmutableMap.Builder<String, DataEntry> values = ImmutableMap.builder();

            int count = 1;

            for (SourceDetails.Property property : this.properties) {
                values.put("P" + count, dataset.hasProperty(property)
                    ? this.session.getDataEntryFactory().create(dataset.getString(property))
                    : DataEntry.NULL_ENTRY
                );

                ++count;
            }

            DataRecord record = new DataRecordImpl(null, null, values.build());

            try {
                boolean result = super.checkExpression(record);
                Outcome outcome = new Outcome(
                    (byte)(result ? 1 : 0), dataset
                );

                if (!result) {
                    count = 1;

                    for (SourceDetails.Property property : this.properties) {
                        outcome.display.put(
                            property.toString(), record.getValue("P" + count).toString()
                        );
                        ++count;
                    }
                }

                results.add(outcome);
            } catch (CorruptRuleException ex) {
                // TODO: This is definitely not the right thing to do here
                //    ...But we're unlikely to trigger this anyway
                throw new RuntimeException(ex);
            }
        }

        return results;
    }
}

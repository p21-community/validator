/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataEntryFactory;
import net.pinnacle21.validator.data.DataRecord;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Tim Stone
 */
public class PercentDifference extends Difference {
    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    PercentDifference(String name, DataEntryFactory factory, String[] arguments) {
        super(name, factory, arguments);
    }

    public DataEntry compute(DataRecord record) {
        DataEntry difference = super.compute(record);
        DataEntry rhs = this.getArgumentValue(record, 1);

        return this.factory.create(
            ((BigDecimal)difference.getValue()).divide(
                (BigDecimal)rhs.getValue(), DIVISION_SCALE, RoundingMode.HALF_UP
            ).multiply(ONE_HUNDRED)
        );
    }
}

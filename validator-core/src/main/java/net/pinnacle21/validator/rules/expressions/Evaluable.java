/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions;

import java.util.Set;

import net.pinnacle21.validator.data.DataRecord;

/**
 * A component of a boolean expression that can be evaluated to be either
 * <code>true</code> or <code>false</code>. This can either be a singular piece of a
 * larger expression, or can represent a compound selection of components.
 * <p>
 * The <code>Evaluable</code> interface only has a single method, <code>evaluate</code>,
 * which provides the result of evaluating the implementation.
 *
 * @author Tim Stone
 */
interface Evaluable {
    /**
     * Evaluates the boolean expression in an implementation-specific way to return a
     * result of <code>true</code> or <code>false</code>.
     *
     * @param record
     * @return the result of evaluating this boolean expression
     */
    public boolean evaluate(DataRecord record);

    /**
     *
     * @return
     */
    public Set<String> getVariables();
}

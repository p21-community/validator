/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import net.pinnacle21.validator.Text;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataEntryFactory;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.expressions.EvaluationException;

import java.math.BigDecimal;

/**
 * @author Tim Stone
 */
public class Difference extends AbstractFunction {
    Difference(String name, DataEntryFactory factory, String[] arguments) {
        super(name, factory, arguments, 2);
    }

    public DataEntry compute(DataRecord record) {
        DataEntry lhs = this.getArgumentValue(record, 0);
        DataEntry rhs = this.getArgumentValue(record, 1);

        if (!lhs.hasValue() || !lhs.isNumeric()) {
            throw new EvaluationException(
                Text.get("Messages.LhsNaN"),
                String.format(
                    Text.get("Descriptions.LhsNaN"),
                    lhs.getDataType().toString()
                )
            );
        }

        if (!rhs.hasValue() || !rhs.isNumeric()) {
            throw new EvaluationException(
                Text.get("Messages.RhsNaN"),
                String.format(
                    Text.get("Descriptions.RhsNaN"),
                    rhs.getDataType().toString()
                )
            );
        }

        return this.factory.create(((BigDecimal)lhs.getValue()).subtract((BigDecimal)rhs.getValue()));
    }
}

/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Tim Stone
 */
public class MatchValidationRule extends AbstractScriptableValidationRule {
    private static final String DEFAULT_DELIMITER = ",";
    private static final String DEFAULT_PAIR_DELIMITER = ":";
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "Variable", "Terms"
    };

    private final String variable;
    private final String pairedVariable;
    private final Map<String, String> acceptableValues = new HashMap<>();
    private boolean isCaseSensitive = true;

    public MatchValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Record, REQUIRED_VARIABLES, metrics);

        this.variable = definition.getProperty("Variable").toUpperCase();
        String delimiter = DEFAULT_DELIMITER;
        String pairDelimiter = DEFAULT_PAIR_DELIMITER;

        super.addVariable(this.variable);

        if (definition.hasProperty("PairedVariable")) {
            this.pairedVariable = definition.getProperty("PairedVariable").toUpperCase();
            super.addVariable(this.pairedVariable);
        } else {
            this.pairedVariable = null;
        }

        if (definition.hasProperty("Delimiter")) {
            delimiter = definition.getProperty("Delimiter");
        }

        if (definition.hasProperty("PairDelimiter")) {
            pairDelimiter = definition.getProperty("PairDelimiter");
        }

        if (definition.hasProperty("CaseSensitive")) {
            if (definition.getProperty("CaseSensitive").equalsIgnoreCase("No")) {
                this.isCaseSensitive = false;
            }
        }

        if (definition.hasProperty("When")) {
            super.prepareExpression(definition.getProperty("When"));
        }

        String[] matchValues = definition.getProperty("Terms").split(
            Pattern.quote(delimiter)
        );

        for (String value : matchValues) {
            if (!this.isCaseSensitive) {
                value = value.toUpperCase();
            }

            String paired = value;

            if (this.pairedVariable != null) {
                String[] pairs = value.split(Pattern.quote(pairDelimiter));

                value = pairs[0];
                paired = pairs[1];
            }

            this.acceptableValues.put(value.trim(), paired.trim());
        }
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        boolean result = true;
        DataEntry entry = dataRecord.getValue(this.variable);

        if (entry.hasValue()) {
            String value = entry.toString();

            if (!this.isCaseSensitive) {
                value = value.toUpperCase();
            }

            if (this.pairedVariable == null) {
                result = this.acceptableValues.containsKey(value);
            } else {
                String expected = this.acceptableValues.get(value);

                entry = dataRecord.getValue(this.pairedVariable);

                if (entry.hasValue()) {
                    value = entry.toString();

                    if (!this.isCaseSensitive) {
                        value = value.toUpperCase();
                    }

                    if (this.acceptableValues.values().contains(value) || expected != null) {
                        result = value.equals(expected);
                    }
                }
            }
        }

        return (byte)(result ? 1 : 0);
    }
}

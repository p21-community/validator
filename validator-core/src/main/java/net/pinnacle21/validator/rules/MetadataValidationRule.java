/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.rules.expressions.PreparedQuery;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.data.LookupProvider;
import net.pinnacle21.validator.report.WritableRuleMetrics;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Tim Stone
 */
public class MetadataValidationRule extends AbstractScriptableValidationRule {
    private static final String[] REQUIRED_VARIABLES = new String[] {
        "From"
    };

    private final boolean ignoreFromFailure;
    private final LookupProvider provider;
    private final PreparedQuery query;

    public MetadataValidationRule(RuleDefinition definition, ValidationSession token,
            WritableRuleMetrics.Scope metrics) throws ConfigurationException {
        super(definition, token, Target.Record, REQUIRED_VARIABLES, metrics);

        if (definition.hasProperty("When")) {
            super.prepareExpression(definition.getProperty("When"));
        }

        this.ignoreFromFailure = definition.hasProperty("Variable") &&
                definition.getProperty("Variable").length() > 0;
        this.provider = this.session.getLookupProvider(null);
        String target = definition.hasProperty("From") ?
                definition.getProperty("From") : null;
        String search = definition.hasProperty("Variable") ?
                definition.getProperty("Variable") : null;
        this.query = new PreparedQuery(target, search);

        for (String variable : this.query.getLocal()) {
            super.addVariable(variable);
        }
    }

    protected byte performValidation(DataRecord dataRecord) throws CorruptRuleException {
        if (!super.checkExpression(dataRecord)) {
            return -1;
        }

        boolean result;
        String target = this.query.getTarget(dataRecord);

        if (this.provider.verifyExists(target)) {
            Set<String> remote = new HashSet<>();

            for (PreparedQuery.Mapping mapping : this.query.getSearch(dataRecord)) {
                remote.add(mapping.getRemote());
            }

            result = this.provider.verifyExists(target, remote);
        } else {
            result = this.ignoreFromFailure;
        }

        return (byte)(result ? 1 : 0);
    }
}

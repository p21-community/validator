/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataEntryFactory;
import net.pinnacle21.validator.data.DataRecord;

/**
 * @author Tim Stone
 */
public class Maximum extends AbstractFunction {
    Maximum(String name, DataEntryFactory factory, String[] arguments) {
        super(name, factory, arguments, 1, true);
    }

    public DataEntry compute(DataRecord record) {
        DataEntry maximum = null;

        for (String variable : this.variables) {
            DataEntry current = record.getValue(variable);

            if (maximum == null || maximum.compareToAny(current) < 0) {
                maximum = current;
            }
        }

        return maximum;
    }
}

/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

/**
 *
 * @author Tim Stone
 */
public class CorruptRuleException extends Exception {
    /**
     *
     * @author Tim Stone
     */
    public enum State {
        Unrecoverable,
        Temporary
    }

    private final String description;
    private final String id;
    private final State state;

    /**
     *
     * @param state
     * @param id
     * @param message
     * @param description
     */
    public CorruptRuleException(State state, String id, String message,
            String description) {
        super(message);

        this.description = description;
        this.id = id;
        this.state = state;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     *
     * @return
     */
    public String getID() {
        return this.id;
    }

    /**
     *
     * @return
     */
    public State getState() {
        return this.state;
    }

    public String toString() {
        return this.getMessage() + ": " + this.getDescription();
    }
}

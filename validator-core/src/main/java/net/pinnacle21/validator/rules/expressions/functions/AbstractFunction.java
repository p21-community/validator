/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import net.pinnacle21.validator.data.DataEntryFactory;
import org.apache.commons.lang3.StringUtils;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.expressions.SyntaxException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tim Stone
 */
public abstract class AbstractFunction implements Function {
    // TODO: Consolidate with definition in Comparison
    private static final String VARIABLE_PATTERN = "[A-Za-z][A-Za-z0-9]*|(?:VAL|SUB|VAR):[A-Za-z0-9]+";

    protected final String name;
    protected final String[] arguments;
    protected final DataEntry[] constants;
    protected final Set<String> variables;
    protected final DataEntryFactory factory;

    public AbstractFunction(String name, DataEntryFactory factory, String[] arguments, int expected) {
        this(name, factory, arguments, expected, false);
    }

    public AbstractFunction(String name, DataEntryFactory factory, String[] arguments, int expected,
            boolean unbounded) {
        if (arguments.length != expected &&
                (!unbounded || arguments.length < expected)) {
            throw new SyntaxException(String.format(
                "The %s function requires %s%d arguments; %d given",
                name, (unbounded ? "at least " : ""), expected, arguments.length
            ));
        }

        this.name = name;
        this.arguments = arguments;
        this.factory = factory;
        this.constants = new DataEntry[arguments.length];

        Set<String> variables = new HashSet<>();

        for (int i = 0; i < this.arguments.length; ++i) {
            String argument = this.arguments[i].trim();
            this.arguments[i] = argument;

            if (this.arguments[i].matches(VARIABLE_PATTERN)) {
                variables.add(this.arguments[i]);
            } else {
                // TODO: We're assuming this is a constant right now
                if (argument.startsWith("'")) {
                    argument = argument.substring(1, argument.length() - 1);
                }

                this.constants[i] = factory.create(argument);
            }
        }

        this.variables = Collections.unmodifiableSet(variables);
    }

    public Set<String> getVariables() {
        return this.variables;
    }

    public String toString() {
        return String.format(
            ":%s(%s)", this.name, StringUtils.join(this.arguments, ',')
        );
    }

    protected DataEntry getArgumentValue(DataRecord record, int argumentIndex) {
        DataEntry value = this.constants[argumentIndex];

        if (value == null) {
            value = record.getValue(this.arguments[argumentIndex]);
        }

        return value;
    }
}

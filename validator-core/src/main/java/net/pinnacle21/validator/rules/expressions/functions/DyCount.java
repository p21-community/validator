/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import net.pinnacle21.validator.Text;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataEntryFactory;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.expressions.EvaluationException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * @author Tim Stone
 */
public class DyCount extends AbstractFunction {
    DyCount(String name, DataEntryFactory factory, String[] arguments) {
        super(name, factory, arguments, 2);
    }

    public DataEntry compute(DataRecord record) {
        DataEntry reference = this.getArgumentValue(record, 0);
        DataEntry recorded = this.getArgumentValue(record, 1);

        if (!reference.hasValue() || !reference.isDate()) {
            throw new EvaluationException(
                Text.get("Messages.InvalidDate"),
                String.format(
                    Text.get("Descriptions.InvalidDate"),
                    this.arguments[0], reference.getDataType().toString()
                )
            );
        }

        if (!recorded.hasValue() || !recorded.isDate()) {
            throw new EvaluationException(
                Text.get("Messages.InvalidDate"),
                String.format(
                    Text.get("Descriptions.InvalidDate"),
                    this.arguments[1], recorded.getDataType().toString()
                )
            );
        }

        LocalDate referenceDate = LocalDate.parse(reference.toString().substring(0, "YYYY-MM-DD".length()),
            DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate recordedDate = LocalDate.parse(recorded.toString().substring(0, "YYYY-MM-DD".length()),
            DateTimeFormatter.ISO_LOCAL_DATE);

        long days = ChronoUnit.DAYS.between(referenceDate, recordedDate);

        return this.factory.create(days < 0 ? days : days + 1);
    }
}

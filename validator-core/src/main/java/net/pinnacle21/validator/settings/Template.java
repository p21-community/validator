/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.util.KeyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Tim Stone
 */
class Template extends Definition {
    private static final Logger LOGGER = LoggerFactory.getLogger(Template.class);

    private static final Map<String, String> TEMPLATE_VARIABLE_MAPPINGS = ImmutableMap.<String, String>builder()
        .put("*", ".+")
        .put("#", "\\d+")
        .put("@", "[A-Za-z]+")
        .build();

    private boolean configured = false;
    private boolean defined = false;
    private final Map<SourceDetails.Reference, Set<RuleDefinition>> rules = new HashMap<>();
    private final Set<Definition> templateVariables = new HashSet<>();
    private final Map<String, Definition> variables = new KeyMap<>();
    private PrototypeCriteria prototypeCriteria;

    Template(String name, PrototypeCriteria prototypeCriteria) {
        super(Definition.Target.Domain, name);

        this.prototypeCriteria = prototypeCriteria;

        for (SourceDetails.Reference target : SourceDetails.Reference.values()) {
            this.rules.put(target, new LinkedHashSet<>());
        }
    }

    Configuration createFrom(String name, Set<String> existing) {
        Configuration configuration = new Configuration(name);

        if (existing != null) {
            // Prepare the incoming variables
            Set<String> variables = new HashSet<>();

            for (String variable : existing) {
                variables.add(variable.toUpperCase());
            }

            for (Definition variable : this.variables.values()) {
                String variableName = this.prepare(variable.getTargetName(), name);

                // Create a copy of the existing variable so we don't mess up the
                // template's version
                Definition replacementVariable = createFrom(
                    variableName,
                    variable
                );

                if (variables.contains(variableName)) {
                    replacementVariable.setProperty("Present", "Y");
                }

                configuration.defineVariable(replacementVariable);

                variables.remove(variableName);
            }

            // Then go ahead and match the template variables, if there are any
            if (!this.templateVariables.isEmpty()) {
                for (String variable : variables) {
                    VariableTemplate template = this.findVariableMatch(variable);

                    if (template != null) {
                        Definition replacementVariable = createFrom(
                            variable, template.template
                        );

                        // TODO: I'm not sure if I like intentionally doing this here...
                        if (replacementVariable.hasProperty("Name")) {
                            replacementVariable.setProperty("Name", variable);
                        }

                        replacementVariable.setProperty("Present", "Y");

                        if (replacementVariable.hasProperty("Label")) {
                            String label = replacementVariable.getProperty("Label");

                            for (int i = 1; i <= template.captures.length; ++i) {
                                label = label.replaceAll(
                                    Pattern.quote("$" + i), template.captures[i - 1]
                                );
                            }

                            replacementVariable.setProperty("Label", label);
                        }

                        configuration.defineVariable(replacementVariable);
                    }
                }
            }
        }

        // Copy over all of the properties
        configuration.properties.putAll(super.properties);

        // TODO: Is this appropriate in all cases?
        // No, it's not, but we should expand on this cleanup at some point...
        if (this.prototypeCriteria != null) {
            String label = this.hasProperty("Label") ? this.getProperty("Label") : name;
            String keys = this.getProperty("DomainKeys");

            configuration.setProperty("Name", name);
            configuration.setProperty("Label", label.replaceAll("%Domain%", name));
            configuration.setProperty("DomainKeys", keys.replaceAll("__", name));
        }

        return configuration;
    }

    void complete() {
        if (this.templateVariables.isEmpty()) {
            return;
        }

        for (Definition variable : this.variables.values()) {
            if (!variable.hasProperty("Config")) {
                VariableTemplate template = this.findVariableMatch(variable.getTargetName());

                if (template != null) {
                    variable.setProperty("Config", "Y");

                    for (String property : template.template.getProperties()) {
                        if (!variable.hasProperty(property)) {
                            variable.setProperty(property, template.template.getProperty(property));
                        }
                    }

                    if (variable.hasProperty("Label")) {
                        String label = variable.getProperty("Label");

                        for (int i = 1; i <= template.captures.length; ++i) {
                            label = label.replaceAll(
                                Pattern.quote("$" + i), template.captures[i - 1]
                            );
                        }

                        variable.setProperty("Label", label);
                    }
                }
            }
        }
    }

    void defineRule(SourceDetails.Reference target, RuleDefinition rule) {
        if (!ConfigurationManager.isValidRuleType(rule.getRuleType())) {
            LOGGER.warn("Refusing to register rule {} because rule type {} is invalid",
                rule.getId(), rule.getRuleType());

            return;
        }

        this.rules.get(target).add(rule);
    }

    void definePrototypeCriteria(PrototypeCriteria prototypeCriteria) {
        this.prototypeCriteria = prototypeCriteria;
    }

    void defineVariable(Definition variable) {
        String variableName = variable.getTargetName();

        for (String templateSequence : TEMPLATE_VARIABLE_MAPPINGS.keySet()) {
            if (variableName.contains(templateSequence)) {
                this.templateVariables.add(variable);

                return;
            }
        }

        this.variables.put(variableName, variable);
    }

    Set<RuleDefinition> getRules(SourceDetails.Reference target) {
        return this.rules.get(target);
    }

    Definition getVariable(String name) {
        return this.variables.get(name);
    }

    boolean hasVariable(String name) {
        return this.variables.containsKey(name);
    }

    boolean isConfiguration() {
        return this.configured;
    }

    boolean isDefinition() {
        return this.defined;
    }

    int matches(String name, Collection<String> datasetVariables) {
        int matches = 0;

        if (this.prototypeCriteria.isFallbackCriteria()) {
            matches = -1;
        } else {
            if (this.prototypeCriteria.hasDatasetName()) {
                // TODO: The pattern swap here is unsafe but #yolo
                if (name.matches(this.prototypeCriteria.getDatasetName().replace("*", ".*"))) {
                    matches = 1;
                }
            }

            if (this.prototypeCriteria.hasVariables()) {
                datasetVariables = datasetVariables.stream()
                    .map(String::trim)
                    .map(String::toUpperCase)
                    .collect(Collectors.toSet());

                for (String variable : this.prototypeCriteria.getVariables()) {
                    boolean isNegativeMatch = variable.startsWith("-");
                    boolean isRegex = variable.contains("*");
                    boolean isFound;

                    if (isNegativeMatch) {
                        variable = variable.substring(1);
                    }

                    variable = this.prepare(variable, name);

                    if (isRegex) {
                        // TODO: Unsafe as above, if someone put in a regex we'd get .* -> ..* etc
                        Pattern pattern = Pattern.compile(variable.replace("*", ".*"));

                        isFound = datasetVariables.stream().anyMatch(s -> pattern.matcher(s).matches());
                    } else {
                        isFound = datasetVariables.contains(variable);
                    }

                    if (isFound) {
                        // A negated match is an immediate disqualification
                        if (isNegativeMatch) {
                            return 0;
                        }

                        // TODO: If there's overlap between a wildcard variable and a normal variable we can get double counting
                        // TODO: This is a fairly hacky way of giving variable matches precedence
                        matches = matches + 10;
                    }
                }
            }
        }

        return matches;
    }

    void markConfigured() {
        this.configured = true;
    }

    void markDefined() {
        this.defined = true;
    }

    void updateVariables(Template target) {
        String targetName = target.getProperty("Name").toUpperCase();

        for (Definition variable : this.variables.values()) {
            String name = this.prepare(variable.getTargetName(), targetName);

            if (target.hasVariable(name)) {
                copyTo(variable, target.getVariable(name));
            } else {
                target.defineVariable(createFrom(name, variable));
            }
        }

        for (Definition variable : this.templateVariables) {
            String name = this.prepare(variable.getTargetName(), targetName);

            target.defineVariable(createFrom(name, variable));
        }
    }

    private String prepare(String variable, String name) {
        return variable.trim().replace("__", name).toUpperCase();
    }

    private VariableTemplate findVariableMatch(String variable) {
        int matches = 0;
        int captured = Integer.MAX_VALUE;
        Definition current = null;
        String[] captures = null;

        for (Definition templateVariable : this.templateVariables) {
            String pattern = templateVariable.getTargetName();

            for (Map.Entry<String, String> replacementSequence : TEMPLATE_VARIABLE_MAPPINGS.entrySet()) {
                pattern = pattern.replace(replacementSequence.getKey(), "(" + replacementSequence.getValue() + ")");
            }

            Matcher matcher = Pattern.compile(pattern).matcher(variable);

            if (matcher.matches()) {
                int count = matcher.groupCount();
                int length = 0;
                String[] groups = new String[count];

                for (int i = 1; i <= count; ++i) {
                    length += (groups[i -1] = matcher.group(i)).length();
                }

                if (count > matches || (count == matches && length < captured)) {
                    matches = count;
                    captured = length;
                    current = templateVariable;
                    captures = groups;
                }
            }
        }

        return current == null ? null : new VariableTemplate(current, captures);
    }

    private static class VariableTemplate {
        final Definition template;
        final String[] captures;

        VariableTemplate(Definition template, String[] captures) {
            this.template = template;
            this.captures = captures;
        }
    }
}

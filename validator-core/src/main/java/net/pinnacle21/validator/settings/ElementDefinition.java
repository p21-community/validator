/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

class ElementDefinition extends Definition {
    ElementDefinition(Target target, Element element) {
        this(target, element,"");
    }

    ElementDefinition(Target target, Element element, String prefix) {
        this(target, element, element.getLocalName(), prefix);
    }

    ElementDefinition(Target target, Element element, String name, String prefix) {
        super(target, name, prefix);

        NamedNodeMap attributes = element.getAttributes();
        int bound = attributes.getLength();

        for (int i = 0; i < bound; ++i) {
            Attr attribute = (Attr)attributes.item(i);

            this.setProperty(attribute.getLocalName(), attribute.getNodeValue());
        }
    }
}

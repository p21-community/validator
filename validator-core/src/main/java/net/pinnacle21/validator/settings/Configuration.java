/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.rules.ValidationRule;
import net.pinnacle21.validator.util.KeyMap;

/**
 * @author Tim Stone
 */
public class Configuration extends Definition {
    private final Set<ValidationRule> filters = new HashSet<>();
    private final Map<SourceDetails.Reference, Set<ValidationRule>> rules = new HashMap<>();
    private Map<String, Definition> variables = new KeyMap<>();

    Configuration(String name) {
        super(Definition.Target.Domain, name);

        for (SourceDetails.Reference target : SourceDetails.Reference.values()) {
            this.rules.put(target, new LinkedHashSet<>());
        }
    }

    public Set<ValidationRule> getRules(SourceDetails.Reference target) {
        return this.rules.get(target);
    }

    public Set<ValidationRule> getFilters() {
        return this.filters;
    }

    public Definition getVariable(String variable) {
        return this.variables.get(variable);
    }

    public Set<Definition> getVariables() {
        return new HashSet<Definition>(this.variables.values());
    }

    public boolean hasVariable(String name) {
        return this.variables.containsKey(name);
    }

    void defineRule(SourceDetails.Reference target, ValidationRule rule) {
        this.rules.get(target).add(rule);
    }

    void defineFilter(ValidationRule filter) {
        this.filters.add(filter);
    }

    void defineVariable(Definition variable) {
        this.variables.put(variable.getTargetName(), variable);
    }
}

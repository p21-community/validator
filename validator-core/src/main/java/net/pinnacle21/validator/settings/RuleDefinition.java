/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.util.KeyMap;
import org.apache.commons.lang3.ObjectUtils;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * @author Tim Stone
 */
public class RuleDefinition {
    private final String id;
    private final String ruleType;
    private final Diagnostic.Type type;
    private final Map<String, String> properties = new KeyMap<>();
    private final List<ConditionalSeverity> conditions = new ArrayList<>();
    private final List<Definition> contexts = new ArrayList<>();
    private final String context;

    public RuleDefinition(@Nonnull String id, @Nonnull String ruleType, @Nonnull Diagnostic.Type type) {
        this(id, ruleType, type, null);
    }

    private RuleDefinition(@Nonnull String id, @Nonnull String ruleType, @Nonnull Diagnostic.Type type, String context) {
        this.id = id;
        this.ruleType = ruleType;
        this.type = type;
        this.context = context;
    }

    private RuleDefinition(@Nonnull RuleDefinition base) {
        this(base, (Diagnostic.Type)null);
    }

    private RuleDefinition(@Nonnull RuleDefinition base, Diagnostic.Type type) {
        this(base.id, base.ruleType, type != null ? type : base.type, base.context);
        this.properties.putAll(base.properties);
        this.conditions.addAll(base.conditions);
        this.contexts.addAll(base.contexts);
    }

    private RuleDefinition(@Nonnull RuleDefinition base, @Nonnull String context) {
        this(base.id, base.ruleType, base.type, context);
        this.properties.putAll(base.properties);
        this.conditions.addAll(base.conditions);
        this.contexts.addAll(base.contexts);
    }

    public @Nonnull String getId() {
        return this.id;
    }

    public @Nonnull String getRuleType() {
        return this.ruleType;
    }

    public @Nonnull Diagnostic.Type getType() {
        return this.type;
    }

    public boolean hasProperty(@Nonnull String property) {
        return this.properties.containsKey(property);
    }

    public String getContext() {
        return this.context;
    }

    public @Nonnull List<Definition> getContexts() {
        return Collections.unmodifiableList(this.contexts);
    }

    public @Nonnull String getProperty(@Nonnull String property) {
        return ObjectUtils.defaultIfNull(this.properties.get(property), "");
    }

    public @Nonnull Set<String> getProperties() {
        return Collections.unmodifiableSet(this.properties.keySet());
    }

    public @Nonnull RuleDefinition withProperty(@Nonnull String property, String value) {
        RuleDefinition definition = this;

        if (!this.hasProperty(property) || !Objects.equals(this.getProperty(property), value)) {
            definition = new RuleDefinition(definition);
            definition.properties.put(property, value);
        }

        return definition;
    }

    public @Nonnull RuleDefinition withConditionalSeverity(@Nonnull ConditionalSeverity condition) {
        RuleDefinition definition = new RuleDefinition(this);

        definition.conditions.add(condition);

        return definition;
    }

    public @Nonnull RuleDefinition withContext(Definition context) {
        RuleDefinition definition = new RuleDefinition(this);

        definition.contexts.add(context);

        return definition;
    }

    public @Nonnull RuleDefinition withContext(String context) {
        return new RuleDefinition(this, context);
    }

    public @Nonnull RuleDefinition withSeverityFor(String domain) {
        ConditionalSeverity substitutedSeverity = null;
        ConditionalSeverity.Match bestMatch = ConditionalSeverity.Match.None;
        ConditionalSeverity.Match match;

        for (ConditionalSeverity condition : this.conditions) {
            match = condition.check(domain, this.context);

            if (match.isBetterThan(bestMatch)) {
                bestMatch = match;
                substitutedSeverity = condition;
            }
        }

        return substitutedSeverity != null
            ? new RuleDefinition(this, substitutedSeverity.getType())
            : this;
    }

    public static class ConditionalSeverity {
        private final String domain;
        private final String context;
        private final Diagnostic.Type type;

        private enum Match {
            None,
            Rule,
            Domain,
            Context,
            Exact;

            boolean isBetterThan(Match match) {
                return match == null || this.ordinal() > match.ordinal();
            }
        }

        public ConditionalSeverity(String domain, String context, @Nonnull Diagnostic.Type type) {
            this.domain = domain;
            this.context = context;
            this.type = type;
        }

        private @Nonnull Match check(String domain, String context) {
            boolean hasDomain = this.domain != null;
            boolean hasContext = this.context != null;
            boolean isValidDomain = !hasDomain || this.domain.equalsIgnoreCase(domain);
            boolean isValidContext = !hasContext || this.context.replace("__", domain).equalsIgnoreCase(context);

            if (hasDomain && isValidDomain && hasContext && isValidContext) {
                return Match.Exact;
            } else if (hasDomain && isValidDomain) {
                return Match.Domain;
            } else if (hasContext && isValidContext) {
                return Match.Context;
            } else if (isValidDomain && isValidContext) {
                return Match.Rule;
            } else {
                return Match.None;
            }
        }

        private @Nonnull Diagnostic.Type getType() {
            return this.type;
        }
    }
}

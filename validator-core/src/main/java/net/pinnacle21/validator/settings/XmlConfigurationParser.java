/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.util.ErrorAccumulator;
import net.pinnacle21.validator.util.Helpers;
import net.pinnacle21.validator.util.KeyMap;
import org.apache.commons.lang3.StringUtils;
import net.pinnacle21.parsing.xml.ElementList;
import net.pinnacle21.parsing.xml.XmlUtils;
import net.pinnacle21.parsing.xml.XmlWriter;
import net.pinnacle21.validator.settings.Definition.Target;
import net.pinnacle21.validator.settings.Codelist.Code;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.pinnacle21.parsing.xml.XmlUtils.each;
import static net.pinnacle21.parsing.xml.XmlUtils.every;
import static net.pinnacle21.parsing.xml.XmlUtils.only;

/**
 * @author Tim Stone
 */
public class XmlConfigurationParser implements ConfigurationParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlConfigurationParser.class);

    private enum DefineVersion {
        V1("http://www.cdisc.org/ns/def/v1.0"),
        V2("http://www.cdisc.org/ns/def/v2.0");

        public final String uri;

        DefineVersion(String uri) {
            this.uri = uri;
        }
    }

    private static final String CONFIG_NAMESPACE_URI =
        "http://www.opencdisc.org/schema/validator";
    private static final String NCI_NAMESPACE_URI = "http://ncicb.nci.nih.gov/xml/odm/EVS/CDISC";
    private static final String GROUPING_SEPARATOR = Character.toString((char)0x1C);
    private static final String PAIR_GROUPING_SEPARATOR = Character.toString((char)0x1F);

    private final Collection<File> configurationFiles;
    private String cwd = "";
    private final File defineFile;
    private final Map<String, String> defaults;
    private final List<ErrorAccumulator> errors = new ArrayList<>();
    private final DocumentBuilder builder;

    public XmlConfigurationParser(Collection<File> configurations, File define, Map<String, String> defaults) {
        this.configurationFiles = configurations;
        this.defineFile = define;
        this.defaults = defaults != null ? defaults : new HashMap<>();

        try {
            this.builder = XmlUtils.newSafeDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            // Shouldn't ever happen
            throw new RuntimeException(ex);
        }
    }

    public List<ErrorAccumulator> getErrors() {
        return Collections.unmodifiableList(this.errors);
    }

    public boolean parse(ConfigurationManager manager) {
        boolean result = true;

        try {
            try {
                // Take care of the define.xml document, if we have one
                if (this.defineFile != null) {
                    ErrorAccumulator errors = new ErrorAccumulator("define",
                            this.defineFile.getName());

                    if (this.defineFile.isFile() && this.defineFile.canRead()) {
                        Document document = this.builder.parse(this.defineFile);

                        // Parse the define.xml document
                        DefineVersion version = this.determineDefineVersion(document);

                        // We know what version this define.xml document is
                        if (version != null) {
                            // Parse the define.xml document
                            this.parseDefineLevelContent(document, true, manager, version,
                                    errors);
                        } else {
                            errors.record("Unable to determine the define.xml document version");
                        }
                    } else {
                        // Check if the define.xml file provided was actually invalid
                        if (!this.defineFile.canRead()) {
                            errors.record("Unable to read file");
                        } else if (!this.defineFile.isFile()) {
                            errors.record("The provided path is not a file");
                        }
                    }

                    if (errors.hasErrors()) {
                        this.errors.add(errors);
                    }
                }
            } catch (SAXParseException | IOException | IllegalArgumentException | SecurityException ex) {
                // Failing to parse the define file should not cause the entire validation to fail.
                // Define Generator should still report the issue so users know what to fix.
                LOGGER.warn("Unable to parse the define.xml file, skipped loading define.xml configurations", ex);
            }

            // Now take care of the configuration documents
            for (File configuration : this.configurationFiles) {
                if (configuration != null) {
                    ErrorAccumulator errors = new ErrorAccumulator("config",
                        configuration.getName());

                    if(configuration.isFile() && configuration.canRead()) {
                        Document document = this.builder.parse(configuration);

                        // Set the current working directory
                        this.cwd = configuration.getParentFile().getAbsolutePath();

                        // Make sure this is a configuration
                        boolean isConfiguration = this.verifyNamespace(document,
                            CONFIG_NAMESPACE_URI);

                        if (isConfiguration) {
                            // Parse the define.xml content from the configuration
                            DefineVersion version = this.determineDefineVersion(document);

                            if (version != null) {
                                boolean parseResult = this.parseDefineLevelContent(document,
                                    false, manager, version, errors);

                                if (!parseResult) {
                                    // TODO: Improve configuration error handling
                                    System.out.println("Issues parsing");
                                }
                            } else {
                                // TODO: Improve configuration error handling
                                System.out.println("We can't determine the version");
                            }

                            // Parse the configuration content from the configuration
                            boolean parseResult =
                                this.parseConfigurationLevelContent(document, manager);

                            if (!parseResult) {
                                // TODO: Improve configuration error handling
                                System.out.println("Issues parsing");
                            }
                        } else {
                            // TODO: Improve configuration error handling
                            System.out.println("Not a configuration");
                        }
                    } else {
                        // Check if the configuration file provided was actually invalid
                        if (configuration.isFile() && !configuration.canRead()) {
                            // TODO: Improve configuration error handling
                            System.out.println("File is not readable");
                        } else if (!configuration.isFile()) {
                            // TODO: Improve configuration error handling
                            System.out.println("This isn't a file / doesn't exist");

                            throw new RuntimeException(String.format(
                                "The configuration file %s does not exist!",
                                configuration.getAbsolutePath()
                            ));
                        }
                    }

                    if (errors.hasErrors()) {
                        this.errors.add(errors);
                    }
                } else {
                    // TODO: Improve configuration error handling
                    System.out.println("Configuration was null");
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }

        if (result) {
            manager.complete();
        }

        return result;
    }

    private boolean parseDefineLevelContent(Document document, boolean isDefineDocument, ConfigurationManager manager,
            DefineVersion version, ErrorAccumulator errors) {
        Element metadata = only("MetaDataVersion", document);

        // TODO: We could have more than one MetaDataVersion section
        //   In what cases is this practical / likely to happen?
        if (metadata == null) {
            errors.record("The number of MetaDataVersion sections was not one");
            // This is an error condition
            return false;
        }

        String contextPrefix = "";

        if (!isDefineDocument) {
            manager.register(new ConfigurationManager.ConfigurationMetadata(
                metadata.getAttributeNS(version.uri, "StandardName"),
                metadata.getAttributeNS(version.uri, "StandardVersion")
            ));

            contextPrefix = metadata.getAttributeNS(CONFIG_NAMESPACE_URI, "Prefix");
        }

        ParseContext context = new ParseContext(version, isDefineDocument, contextPrefix);

        this.parseCodeAndValueLists(context, metadata, errors);

        // Now get the variables
        ElementList itemDefs = every("ItemDef", metadata);

        if (!itemDefs.hasElements()) {
            // TODO: Improve configuration error handling
            System.out.println("There should be ItemDef elements in the document");

            // This is an error condition
            return false;
        }

        this.parseItemDefs(context, itemDefs);

        Map<String, PrototypeCriteria> prototypes = new KeyMap<>();

        // Check for prototypes, but not if this is define.xml
        if (!isDefineDocument) {
            for (Element prototypeDef : every(CONFIG_NAMESPACE_URI,"Prototype", metadata)) {
                String oid = prototypeDef.getAttribute("ItemGroupOID");
                String name = prototypeDef.getAttribute("DatasetName");
                String[] keys = Helpers.trimSplit(prototypeDef.getAttribute("KeyVariables"), ",");

                prototypes.put(oid, new PrototypeCriteria(name, keys));
            }
        }

        // Now get the ItemGroupDefs
        ElementList itemGroupDefs = every("ItemGroupDef", metadata);

        if (!itemGroupDefs.hasElements()) {
            // TODO: Improve configuration error handling
            System.out.println("There should be ItemGroupDefs elements in the document");

            // This is an error condition
            return false;
        }

        return this.parseItemGroupDefs(context, manager, itemGroupDefs, prototypes);
    }

    private void parseItemDefs(ParseContext context, ElementList itemDefs) {
        List<ValueList.Resolver> resolvers = new ArrayList<>();

        for (Element itemDef : itemDefs) {
            String oid = itemDef.getAttribute("OID");
            Definition definition = new ElementDefinition(
                Target.Variable,
                itemDef,
                itemDef.getAttribute("Name"),
                context.isDefaultConfig ? "" : context.prefix
            );

            if (definition.hasProperty("DataType")) {
                String type = definition.getProperty("DataType");
                String basicType = "Char";
                String regexType = ".*";

                // Check if the basic type (references a SAS transport type) is numeric
                if (type.equalsIgnoreCase("Integer") || type.equalsIgnoreCase("Float")) {
                    basicType = "Num";
                }

                // TODO: This seems...very hackish. Should we move it to the rule?
                //   I also think that some of these regular expressions might be a bit
                //   off. We should look into that too...
                if (type.equalsIgnoreCase("DateTime")) {
                    regexType = "^(-|[0-9]{4})(-(-|0[0-9]|1[0-2])"
                            + "(-(-|[0-2][0-9]|3[0-1])(T(-|[0-1][0-9]|2[0-3])"
                            + "(:(-|[0-5][0-9])(:[0-5][0-9])?)?)?)?)?(/(-|[0-9]{4})"
                            + "(-(-|0[0-9]|1[0-2])(-(-|[0-2][0-9]|3[0-1])"
                            + "(T(-|[0-1][0-9]|2[0-3])(:(-|[0-5][0-9])"
                            + "(:[0-5][0-9])?)?)?)?)?)?$";
                } else if (type.equalsIgnoreCase("Date")) {
                    regexType = "^(-|[0-9]{4})(-(-|0[0-9]|1[0-2])"
                            + "(-(-|[0-2][0-9]|3[0-1]))?)?(/(-|[0-9]{4})"
                            + "(-(-|0[0-9]|1[0-2])(-(-|[0-2][0-9]|3[0-1]))?)?)?$";
                } else if (type.equalsIgnoreCase("Time")) {
                    regexType = "^T(-|[0-1][0-9]|2[0-3])(:(-|[0-5][0-9])"
                            + "(:[0-5][0-9])?)?(/T(-|[0-1][0-9]|2[0-4])"
                            + "(:(-|[0-5][0-9])(:[0-5][0-9])?)?)?$";
                } else if (type.equalsIgnoreCase("Integer")) {
                    regexType = "-?[0-9]+";
                } else if (type.equalsIgnoreCase("Float")) {
                    regexType = "-?[0-9]+(\\.[0-9]+)?";
                }

                definition.setProperty("Type", type);
                definition.setProperty("Type.Basic", basicType);
                definition.setProperty("Type.Regex", regexType);
            }

            ElementList codelistRefs = every("CodeListRef", itemDef);

            if (context.version == DefineVersion.V2) {
                definition.setProperty("Label", readDescription(itemDef));
            }

            definition.clearPrefix();
            definition.setProperty(context.prefix, "Y");

            if (!context.isDefine) {
                definition.setPrefix(context.prefix);
            }

            // Check for a code list reference
            if (codelistRefs.hasElements()) {
                Element codelistRef = codelistRefs.get(0);
                Codelist codelist = context.getCodeList(
                    codelistRef.getAttribute("CodeListOID").toUpperCase()
                );

                if (codelist != null) {
                    if (!codelist.isExternal()) {
                        StringBuilder codes = new StringBuilder();
                        StringBuilder decodes = new StringBuilder();
                        boolean first = true;

                        for (Code code : codelist) {
                            if (!first) {
                                codes.append(GROUPING_SEPARATOR);

                                if (decodes.length() > 0) {
                                    decodes.append(GROUPING_SEPARATOR);
                                }
                            }

                            codes.append(code.getValue());

                            if (code.hasDecode()) {
                                decodes.append(code.getValue())
                                    .append(PAIR_GROUPING_SEPARATOR)
                                    .append(code.getDecode());
                            }

                            first = false;
                        }

                        definition.setProperty("CodeList", "Y");
                        definition.setProperty("CodeList.Name", codelist.getName());
                        definition.setProperty("CodeList.Values", codes.toString());
                        definition.setProperty("CodeList.Delimiter", GROUPING_SEPARATOR);
                        definition.setProperty("CodeList.PairDelimiter", PAIR_GROUPING_SEPARATOR);
                        definition.setProperty("CodeList.Extensible", codelist.isExtensible() ? "Y" : "N");

                        if (decodes.length() > 0) {
                            definition.setProperty("CodeList.PairedValues", decodes.toString());
                        }

                        if (context.isDefine) {
                            // TODO: This is a legacy property
                            definition.setProperty("Define.WithCodeList", "Y");
                        }
                    } else {
                        definition.setProperty("CodeList.External", "Y");
                        definition.setProperty("CodeList.Name", codelist.getName());
                        definition.setProperty("CodeList.Dictionary",
                                codelist.getDictionary());
                        definition.setProperty("CodeList.Version", codelist.getVersion());
                    }
                } else {
                    // TODO: Improve configuration error handling
                    System.out.println(String.format(
                        "The ItemDef %s contains a reference to the unknown codelist %s",
                        oid, codelistRef.getAttribute("CodeListOID")
                    ));
                }
            }

            if (context.version == DefineVersion.V2) {
                // TODO: Complain if they have more than one, since this returns null in that case too
                Element valueListRef = only(context.version.uri, "ValueListRef", itemDef);

                if (valueListRef != null) {
                    ValueList valueList = context.getValueList(
                        valueListRef.getAttribute("ValueListOID").toUpperCase()
                    );

                    if (valueList != null) {
                        resolvers.add(valueList.resolverFor(definition));
                    } else {
                        System.out.println(String.format(
                            "The ItemDef %s contains a reference to the unknown value list %s",
                            oid, valueListRef.getAttribute("ValueListOID")
                        ));
                    }
                }
            }

            definition.clearPrefix();

            context.add(oid, definition);
        }

        // Now we go back and resolve and value lists since they require us to have read all the ItemDefs first
        for (ValueList.Resolver resolver : resolvers) {
            if (!resolver.resolve(context.variables)) {
                // TODO: Improve configuration error handling
                System.out.println(String.format("Unable to fully resolve value list definition %s for %s",
                    resolver.getValueList().getOid(), resolver.getVariable().getTargetName()));
            }
        }
    }

    private boolean parseItemGroupDefs(ParseContext context, ConfigurationManager manager, ElementList itemGroupDefs,
            Map<String, PrototypeCriteria> prototypes) {
        boolean result = true;

        for (Element itemGroupDef : itemGroupDefs) {
            Template template = null;
            String name = itemGroupDef.getAttribute("Name");
            String oid = itemGroupDef.getAttribute("OID");

            // Check if define.xml already specified this configuration
            if (manager.defines(name)) {
                template = manager.getConfiguration(name);
            }

            boolean isPrototype = prototypes.containsKey(oid);

            // Do some sanity checking
            if (isPrototype) {
                // This should always be true
                if (template == null) {
                    template = new Template(name, prototypes.get(oid));
                } else {
                    // TODO: Improve configuration error handling
                    System.out.println(String.format(
                        "The ItemGroupDef named %s conflicts with a configuration prototype" +
                        " (oid: %s). This shouldn't happen!",
                        name, oid
                    ));

                    // TODO: I don't understand how people manage to produce the scenario where this is needed
                    template.definePrototypeCriteria(prototypes.get(oid));
                }
            } else if (template == null) {
                template = new Template(name, null);
            }

            Definition current = new ElementDefinition(Target.Domain, itemGroupDef);

            if (context.version == DefineVersion.V2) {
                current.setProperty("Label", readDescription(itemGroupDef));
            }

            Definition.copyTo(current, template, "DomainKeys");

            if (context.isDefine) {
                template.setProperty("Define", "Y");
                template.markDefined();

                String keys = cleanDomainKeys(itemGroupDef.getAttribute("DomainKeys"));

                if (keys != null) {
                    template.setProperty("Define.DomainKeys", keys);
                }
            } else {
                template.setProperty("Config", "Y");
            }

            SortedMap<Integer, String> keyVariables = new TreeMap<>();

            for (Element itemRef : every("ItemRef", itemGroupDef)) {
                String referenceOID = itemRef.getAttribute("ItemOID");
                Definition reference = new ElementDefinition(Target.Variable, itemRef,
                    context.isDefaultConfig ? "" : context.prefix);
                Definition definition = context.getVariable(referenceOID);

                if (definition == null) {
                    // TODO: Improve configuration error handling
                    System.out.println(String.format("The ItemGroupDef %s contains a reference to an unknown ItemDef %s",
                        name, referenceOID));

                    result = false;

                    // Move on to the next one
                    continue;
                }

                // Being forced to do this is dumb
                reference.clearPrefix();

                Definition variable = Definition.createFrom(definition, reference);
                String variableName = variable.getTargetName();

                if (template.hasVariable(variableName)) {
                    Definition copy = Definition.createFrom(
                        template.getVariable(variableName),
                        variable
                    );

                    if (!context.isDefine && variable.hasProperty("OrderNumber")) {
                        copy.setProperty(context.prefix + ".OrderNumber", variable.getProperty("OrderNumber"));
                    }

                    variable = copy;
                } else if (!context.isDefine && variable.hasProperty("OrderNumber")) {
                    variable.setProperty(context.prefix + ".OrderNumber", variable.getProperty("OrderNumber"));
                }

                if (context.version == DefineVersion.V2 && variable.hasProperty("KeySequence")) {
                    try {
                        keyVariables.put(Integer.valueOf(variable.getProperty("KeySequence")), variableName);
                    } catch (NumberFormatException ignore) {}
                }

                template.defineVariable(variable);
            }

            if (!keyVariables.isEmpty()) {
                template.setProperty((context.isDefaultConfig ? "" : context.prefix) + "DomainKeys",
                    String.join(",", keyVariables.values()));
            }

            if (!isPrototype) {
                manager.define(template);
            } else {
                manager.prototype(template);
            }
        }

        return result;
    }

    private void parseCodeAndValueLists(ParseContext context, Element metadata, ErrorAccumulator errors) {
        this.parseCodeListElements(context, every("CodeList", metadata), errors);

        if (context.version == DefineVersion.V2) {
            this.parseValueListElements(
                context,
                every(DefineVersion.V2.uri, "ValueListDef", metadata),
                every(DefineVersion.V2.uri, "WhereClauseDef", metadata),
                errors
            );
        }

        if (context.isDefine) {
            return;
        }

        for (Element terminologyReference : every(CONFIG_NAMESPACE_URI, "TerminologyRef", metadata)) {
            String path = this.replaceSystemProperties(
                terminologyReference.getAttributeNS(XmlWriter.XLINK_NAMESPACE_URI, "href")
            );

            File include = new File(path);

            if (!include.isAbsolute()) {
                include = new File(this.cwd, path);
            }

            if (include.isFile() && include.canRead()) {
                try {
                    Document document = this.builder.parse(include);

                    Element metaDataVersionElement = only("MetaDataVersion", document);

                    if (metaDataVersionElement == null) {
                        // TODO: Improve configuration error handling
                        System.out.println("There are not the right number of MetaDataVersion "
                            + "sections");

                        // This is an error condition
                        continue;
                    }

                    this.parseCodeListElements(context, every("CodeList", metaDataVersionElement), errors);
                    this.parseValueListElements(
                        context,
                        every(DefineVersion.V2.uri, "ValueListDef", metaDataVersionElement),
                        every(DefineVersion.V2.uri, "WhereClauseDef", metaDataVersionElement),
                        errors
                    );
                    this.parseItemDefs(context, every("ItemDef", metaDataVersionElement));
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            } else {
                errors.record(String.format("%s can't be read / does not exist",
                    include.getAbsolutePath()));
            }
        }
    }

    private void parseCodeListElements(ParseContext context, ElementList codelistDefs, ErrorAccumulator errors) {
        for (Element codelistDef : codelistDefs) {
            Codelist codelist = null;
            String name = codelistDef.getAttribute("Name");
            String type = codelistDef.getAttribute("DataType");
            String oid = codelistDef.getAttribute("OID").toUpperCase();
            String extensible = codelistDef.getAttributeNS(NCI_NAMESPACE_URI, "CodeListExtensible");

            errors.startContext("CodeList", oid);

            ElementList codeDefs = every("CodeListItem", codelistDef);

            if (!codeDefs.hasElements() && context.version == DefineVersion.V2) {
                codeDefs = every("EnumeratedItem", codelistDef);
            }

            if (codeDefs.hasElements()) {
                codelist = new Codelist(name, type, oid, "yes".equalsIgnoreCase(extensible));

                for (Element codeDef : codeDefs) {
                    Code code = new Code(codeDef.getAttribute("CodedValue"));

                    for (Element decodeDef : every("Decode", codeDef)) {
                        Element translatedText = only("TranslatedText", decodeDef);

                        if (translatedText == null) {
                            continue;
                        }

                        String lang = translatedText.getAttributeNS(
                            XmlWriter.XML_NAMESPACE_URI, "lang"
                        );

                        if (StringUtils.isEmpty(lang)) {
                            lang = "en";
                        }

                        code.setDecode(translatedText.getTextContent(), lang);
                    }

                    codelist.addCode(code);
                }
            } else {
                codeDefs = every("ExternalCodeList", codelistDef);

                if (codeDefs.hasElements()) {
                    if (codeDefs.size() == 1) {
                        Element codeDef = codeDefs.get(0);

                        codelist = new Codelist(
                            name,
                            type,
                            oid,
                            codeDef.getAttribute("Dictionary"),
                            codeDef.getAttribute("Version")
                        );
                    } else {
                        errors.record("There are multiple ExternalCodeList elements");
                    }
                }
            }

            if (codelist != null) {
                context.add(codelist);
            } else {
                errors.record("The codelist lacks valid children elements");
            }

            errors.endContext();
        }
    }

    private void parseValueListElements(ParseContext context, ElementList valueListDefs, ElementList whereClauseDefs,
            ErrorAccumulator errors) {
        Map<String, Element> whereClauses = new HashMap<>();

        for (Element whereClauseDef : whereClauseDefs) {
            whereClauses.put(whereClauseDef.getAttribute("OID"), whereClauseDef);
        }

        outer: for (Element valueListDef : valueListDefs) {
            ValueList valueList = new ValueList(valueListDef.getAttribute("OID"));

            for (Element itemRef : every("ItemRef", valueListDef)) {
                ValueList.Clause clause = valueList.addClause(itemRef.getAttribute("ItemOID"),
                    itemRef.getAttribute("Mandatory"));

                Element whereClauseRef = only(DefineVersion.V2.uri, "WhereClauseRef", itemRef);

                if (whereClauseRef == null) {
                    errors.record(String.format("ValueList %s ItemRef %s does not contain a WhereClauseRef",
                        valueListDef.getAttribute("OID"), itemRef.getAttribute("ItemOID")));

                    continue outer;
                }

                Element whereClauseDef = whereClauses.get(whereClauseRef.getAttribute("WhereClauseOID"));

                if (whereClauseDef == null) {
                    errors.record(String.format("No WhereClauseDef with OID %s exists",
                        whereClauseRef.getAttribute("WhereClauseOID")));

                    continue outer;
                }

                for (Element rangeCheck : every("RangeCheck", whereClauseDef)) {
                    Set<String> values = new HashSet<>();

                    for (Element checkValue : every("CheckValue", rangeCheck)) {
                        values.add(checkValue.getTextContent());
                    }

                    clause.addCheck(
                        rangeCheck.getAttributeNS(DefineVersion.V2.uri, "ItemOID"),
                        rangeCheck.getAttribute("Comparator"),
                        values
                    );
                }
            }

            context.add(valueList);
        }
    }

    private boolean parseConfigurationLevelContent(Document document, ConfigurationManager manager) {
        Element metaDataVersionElement = only("MetaDataVersion", document);

        if (metaDataVersionElement == null) {
            // TODO: Improve configuration error handling
            LOGGER.warn("Expected to find one and only one MetaDataVersion section");

            // This is an error condition
            return false;
        }

        Element validationRuleElement = only(CONFIG_NAMESPACE_URI, "ValidationRules", metaDataVersionElement);

        if (validationRuleElement == null) {
            // TODO: Improve configuration error handling
            LOGGER.warn("Expected to find one and only one ValidationRules section");

            return true;
        }

        Map<String, RuleDefinition> rules = new KeyMap<>();

        for (Element ruleElement : each(validationRuleElement.getChildNodes())) {
            String id = ruleElement.getAttribute("ID");
            String type = ruleElement.getLocalName();

            if (rules.containsKey(id)) {
                LOGGER.warn("Cannot redefine rule {}", id);

                continue;
            }

            Diagnostic.Type severity = Diagnostic.Type.fromString(ruleElement.getAttribute("Type"));
            RuleDefinition rule = new RuleDefinition(id, type, severity);

            for (Element conditionElement : every(CONFIG_NAMESPACE_URI, "Condition", ruleElement)) {
                String domain = StringUtils.defaultIfBlank(conditionElement.getAttribute("ItemGroupDef"), null);
                String context = StringUtils.defaultIfBlank(conditionElement.getAttribute("Context"), null);
                severity = Diagnostic.Type.fromString(conditionElement.getAttribute("Type"));

                rule = rule.withConditionalSeverity(new RuleDefinition.ConditionalSeverity(domain, context, severity));
            }

            for (Node attribute : each(ruleElement.getAttributes())) {
                String name = attribute.getLocalName();

                if (!name.equals("ID")) {
                    rule = rule.withProperty(name, this.replaceSystemProperties(attribute.getNodeValue()));
                }
            }

            // TODO: Refactor manager.store() at some later point
            manager.store(new ElementDefinition(Target.Rule, ruleElement));

            rules.put(id, rule);
        }

        for (Element itemGroupDefElement : every("ItemGroupDef", metaDataVersionElement)) {
            String name = itemGroupDefElement.getAttribute("Name");
            Template itemGroupDef;

            if (manager.defines(name)) {
                itemGroupDef = manager.getConfiguration(name);
            } else if (manager.prototypes(name)) {
                itemGroupDef = manager.getPrototype(name);
            } else {
                // We didn't opt to create a configuration for this ItemGroupDef
                continue;
            }

            NodeList validationRuleRefs = itemGroupDefElement.getElementsByTagNameNS(CONFIG_NAMESPACE_URI,
                "ValidationRuleRef");

            if (validationRuleRefs.getLength() == 0) {
                // Skip ItemGroupDefs without any rules referenced
                continue;
            }

            for (Element validationRuleRef : each(validationRuleRefs)) {
                this.parseConfigurationRule(rules, new ElementDefinition(Target.Rule, validationRuleRef), itemGroupDef);
            }

            itemGroupDef.markConfigured();
        }

        return true;
    }

    private void parseConfigurationRule(Map<String, RuleDefinition> rules, Definition reference, Template template) {
        String id = reference.getProperty("RuleID");
        boolean isActive = !reference.getProperty("Active").equalsIgnoreCase("No");
        boolean isMetadata = reference.getProperty("Target").equalsIgnoreCase("Metadata");

        RuleDefinition rule = rules.get(id);

        if (rule == null) {
            LOGGER.warn("ItemGroupDef {} references unknown rule {}", template.getTargetName(), id);

            return;
        }

        if (!isActive || rule.getProperty("Active").equalsIgnoreCase("No")) {
            return;
        }

        if (!isMetadata) {
            isMetadata = rule.getProperty("Target").equalsIgnoreCase("Metadata");
        }

        SourceDetails.Reference target = isMetadata
            ? SourceDetails.Reference.Metadata
            : SourceDetails.Reference.Data;

        template.defineRule(target, rule);
    }

    private String replaceSystemProperties(String value) {
        value = value.replace(
            "%System.ConfigDirectory%", this.cwd
        );

        for (Map.Entry<String, String> property : this.defaults.entrySet()) {
            value = value.replaceAll("(?i)%" + Pattern.quote("System." + property.getKey()) + "%",
                Matcher.quoteReplacement(property.getValue()));
        }

        return value;
    }

    private String readDescription(Element element) {
        Element chosenDescription = null;

        // TODO: only() doesn't work as expected here because Description can appear in subelements too
        // The behaviour might need to automatically apply to only direct children but that might break elsewhere?
        for (Element description : every("Description", element)) {
            if (description.getParentNode().isSameNode(element)) {
                chosenDescription = description;

                break;
            }
        }

        if (chosenDescription == null) {
            return "";
        }

        // TODO: This isn't correct if they have multiple TranslatedTexts...
        Element text = only("TranslatedText", chosenDescription);

        return text.getTextContent();
    }

    private DefineVersion determineDefineVersion(Document document) {
        DefineVersion version = null;

        if (this.verifyNamespace(document, DefineVersion.V1.uri)) {
            version = DefineVersion.V1;
        } else if (this.verifyNamespace(document, DefineVersion.V2.uri)) {
            version = DefineVersion.V2;
        }

        return version;
    }

    private boolean verifyNamespace(Document document, String namespaceURI) {
        boolean result = false;

        if (document != null) {
            String namespacePrefix = document.lookupPrefix(namespaceURI);

            if (namespacePrefix != null) {
                result = true;
            }
        }

        return result;
    }

    private static String cleanDomainKeys(String raw) {
        String[] pieces = raw.split("\\W+");

        if (pieces.length == 1 || StringUtils.isEmpty(pieces[0])) {
            return null;
        }

        return StringUtils.join(pieces, ",");
    }

    private static class ParseContext {
        private static final String DEFAULT_PREFIX = "Config";

        private final Map<String, Codelist> codelists = new HashMap<>();
        private final Map<String, ValueList> valueLists = new HashMap<>();
        private final Map<String, Definition> variables = new KeyMap<>();
        private final DefineVersion version;
        private final String prefix;
        private final boolean isDefine;
        private final boolean isDefaultConfig;

        ParseContext(DefineVersion version, boolean isDefine, String prefix) {
            this.version = version;
            this.prefix = StringUtils.defaultIfEmpty(prefix, isDefine ? "Define" : "Config");
            this.isDefine = isDefine;
            this.isDefaultConfig = this.prefix.equals(DEFAULT_PREFIX);
        }

        void add(Codelist codelist) {
            this.codelists.put(codelist.getOID(), codelist);
        }

        void add(ValueList valueList) {
            this.valueLists.put(valueList.getOid(), valueList);
        }

        void add(String oid, Definition variable) {
            this.variables.put(oid, variable);
        }

        Codelist getCodeList(String oid) {
            return this.codelists.get(oid);
        }

        ValueList getValueList(String oid) {
            return this.valueLists.get(oid);
        }

        Definition getVariable(String oid) {
            return this.variables.get(oid);
        }
    }
}

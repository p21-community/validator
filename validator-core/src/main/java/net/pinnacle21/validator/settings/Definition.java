/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import java.util.*;

import net.pinnacle21.validator.util.KeyMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Stores a <code>Map</code> of <code>String</code> based properties, used for passing
 * around information within <code>ConfigurationManager</code> implementations, and for
 * creating <code>ValidationRule</code> instances.
 *
 * @author Tim Stone
 */
public class Definition {
    /**
     *
     * @author Tim Stone
     */
    public enum Target {
        Domain,
        Filter,
        Rule,
        Variable
    }

    protected final Map<String, String> properties = new KeyMap<String>();
    protected final Set<Definition> dependencies = new HashSet<Definition>();

    private final Set<String> usedPrefixes = new HashSet<String>();

    private final Target target;
    private final String targetName;
    private String prefix;

    public Definition(Target target, String targetName) {
        this(target, targetName, "");
    }

    public Definition(Target target, String targetName, String prefix) {
        this.target = target;
        this.targetName = targetName;
        this.setPrefix(prefix);
    }

    public static void copyTo(Definition source, Definition destination) {
        copyTo(source, destination, false);
    }

    public static void copyTo (Definition source, Definition destination, boolean stripPrefxies) {
        copyTo(source, destination, stripPrefxies, (String[]) null);
    }

    public static void copyTo(Definition source, Definition destination, String... forceCopy) {
        copyTo(source, destination, false, forceCopy);
    }

    public static void copyTo(Definition source, Definition destination, boolean stripPrefixes, String... forceCopy) {
        Set<String> forcedProperties = new HashSet<String>();

        if (forceCopy != null) {
            for (String property : forceCopy) {
                forcedProperties.add(property.toUpperCase());
            }
        }

        for (String property : source.getProperties()) {
            String targetProperty = property;

            if (stripPrefixes) {
                int index = property.indexOf('.');

                if (index != -1) {
                    String prefix = property.substring(0, index);

                    if (source.usedPrefixes.contains(prefix)) {
                        targetProperty = property.substring(index + 1);
                    }
                }
            }

            if (!destination.hasProperty(targetProperty) || forcedProperties.contains(property)) {
                destination.setProperty(targetProperty, source.getProperty(property));
            }
        }

        destination.dependencies.addAll(source.dependencies);
    }

    public static Definition createFrom(Definition definition) {
        return createFrom(definition.getTargetName(), definition);
    }

    public static Definition createFrom(Definition...definitions) {
        return createFrom(definitions[0].getTargetName(), definitions);
    }

    public static Definition createFrom(String targetName, Definition...definitions) {
        return createFrom(targetName, false, definitions);
    }

    public static Definition createFrom(String targetName, boolean stripPrefixes, Definition...definitions) {
        Target target = definitions[0].getTarget();
        Definition combination = new Definition(target, targetName);

        for (Definition definition : definitions) {
            if (definition.getTarget() != target) {
                throw new IllegalArgumentException("Definitions of varying targets "
                        + "cannot be combined");
            }

            copyTo(definition, combination, stripPrefixes);
        }

        return combination;
    }

    Definition with(String prefix, Definition definition) {
        Definition target = createFrom(this);

        target.setPrefix(prefix);
        copyTo(definition, target);
        target.clearPrefix();

        return target;
    }

    boolean hasDependencies() {
        return !this.dependencies.isEmpty();
    }

    Definition addDependency(Definition dependency) {
        this.dependencies.add(dependency);

        return this;
    }

    public void clearPrefix() {
        this.setPrefix("");
    }

    public Set<Definition> getDependencies() {
        // TODO: The individual Definition objects in this set should also be made unmodifiable
        return Collections.unmodifiableSet(this.dependencies);
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getProperty(String property) {
        if (!this.hasProperty(property)) {
            return "";
        }

        return this.properties.get(this.prefix(property));
    }

    public Set<String> getProperties() {
        return this.properties.keySet();
    }

    public Target getTarget() {
        return this.target;
    }

    public String getTargetName() {
        return this.targetName;
    }

    public boolean hasProperty(String property) {
        return this.properties.containsKey(this.prefix(property));
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;

        if (StringUtils.isNotEmpty(prefix)) {
            this.usedPrefixes.add(prefix.toUpperCase());
        }
    }

    public Definition setProperty(String property, String value) {
        if (value != null && value.length() > 0) {
            this.properties.put(this.prefix(property), value);
        }

        return this;
    }

    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this)
                .append(this.target)
                .append(this.targetName);

        for (String property : this.properties.keySet()) {
            builder.append(property, this.properties.get(property));
        }

        return builder.toString();
    }

    private String prefix(String property) {
        if (StringUtils.isEmpty(this.prefix)) {
            return property;
        }

        return String.format("%s.%s", this.prefix, property);
    }
}

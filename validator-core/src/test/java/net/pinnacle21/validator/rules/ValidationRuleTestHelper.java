/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.data.DataRecordImpl;
import net.pinnacle21.validator.report.WritableRuleMetrics;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author Tim Stone
 */
public class ValidationRuleTestHelper {
    public static final WritableRuleMetrics.Scope MOCK_SCOPE = mock(WritableRuleMetrics.Scope.class);

    static {
        when(MOCK_SCOPE.getRule(anyString(), anyString(), anyString(), any())).thenReturn(new WritableRuleMetrics.RuleMetric() {
            @Override
            public void start() {

            }

            @Override
            public void stop(boolean executed, boolean failed) {

            }

            @Override
            public String getMessage() {
                return null;
            }

            @Override
            public Diagnostic.Type getType() {
                return null;
            }

            @Override
            public SourceDetails.Reference getReference() {
                return null;
            }

            @Override
            public int getInvocations() {
                return 0;
            }

            @Override
            public int getExecutions() {
                return 0;
            }

            @Override
            public int getFailures() {
                return 0;
            }

            @Override
            public long getElapsed() {
                return 0;
            }
        });
    }

    /**
     * Creates the specified number of <code>DataRecord</code> objects.
     *
     * @param recordValues  the value maps of the records to create
     * @return  the generated <code>List</code> of <code>DataRecord</code> objects
     */
    public static List<DataRecord> createRecords(List<ImmutableMap<String, DataEntry>> recordValues) {
        List<DataRecord> records = new ArrayList<>(recordValues.size());

        for (ImmutableMap<String, DataEntry> values : recordValues) {
            records.add(new DataRecordImpl(null, null, values));
        }

        return records;
    }

    /**
     * Creates as many <code>DataRecord</code> objects as there are passed <code>DataEntry</code> objects,
     * setting each <code>DataEntry</code> as the specified <code>variable</code> on the respective record.
     *
     * @param variable  the variable to put the entry into for each record
     * @param entries  the entries to populate the <code>DataRecord</code> objects with
     * @return the generated <code>List</code> of <code>DataRecord</code> objects
     */
    public static List<DataRecord> createRecords(String variable, DataEntry... entries) {
        List<ImmutableMap<String, DataEntry>> recordValues = new ArrayList<>(entries.length);

        for (DataEntry entry : entries) {
            recordValues.add(ImmutableMap.of(variable, entry));
        }

        return createRecords(recordValues);
    }

    public static List<DataRecord> createRecords(String[] variables, DataEntry[]... entries) {
        List<ImmutableMap<String, DataEntry>> recordValues = new ArrayList<>(entries.length);

        for (DataEntry[] entry : entries) {
            ImmutableMap.Builder<String, DataEntry> values = ImmutableMap.builder();

            for (int i = 0; i < variables.length; ++i) {
                values.put(variables[i], entry[i]);
            }

            recordValues.add(values.build());
        }

        return createRecords(recordValues);
    }
}

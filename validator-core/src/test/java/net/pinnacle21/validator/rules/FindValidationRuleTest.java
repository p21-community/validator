/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import net.pinnacle21.validator.api.model.CancellationToken;
import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import org.junit.Test;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.settings.ConfigurationException;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.ValidationSession;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static net.pinnacle21.validator.rules.ValidationRuleTestHelper.MOCK_SCOPE;

/**
 * @author Tim Stone
 */
public class FindValidationRuleTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);
    private final ValidationSession session = ValidationSession.create(this.options, new CancellationToken(),
            this.factory, null);

    /**
     * Assertion: You cannot specify both Terms and Test for a Find rule.
     */
    @Test(expected = ConfigurationException.class)
    public void TermsAndTestNotBothAllowed() throws ConfigurationException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getId()).thenReturn("XX");
        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.hasProperty("Test")).thenReturn(true);

        new FindValidationRule(definition, this.session, MOCK_SCOPE);
    }

    /**
     * Assertion: You must specify either Terms or Test for a Find rule.
     */
    @Test(expected = ConfigurationException.class)
    public void TermsOrTestMustBePresent() throws ConfigurationException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getId()).thenReturn("XX");
        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.hasProperty("Terms")).thenReturn(false);
        when(definition.hasProperty("Test")).thenReturn(false);

        new FindValidationRule(definition, this.session, MOCK_SCOPE);
    }

    /**
     * Assertion: If all Terms are present in the data, a Find rule should pass.
     */
    @Test
    public void AllTermsMatchSuccess() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("A,C");

        ValidationRule rule = new FindValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("B"),
            this.factory.create("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }

    /**
     * Assertion: If Match is specified and <code>n</code> Terms are present in the data,
     *     a Find rule should pass.
     */
    @Test
    public void TwoTermsMatchSuccess() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("A,B,C");
        when(definition.hasProperty("Match")).thenReturn(true);
        when(definition.getProperty("Match")).thenReturn("2");

        ValidationRule rule = new FindValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("B"),
            this.factory.create("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }

    /**
     * Assertion: If Match is specified and less than <code>n</code> Terms are present in
     *     the data, a Find rule should fail.
     */
    @Test
    public void TwoTermsMatchFailure() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("A,B,C");
        when(definition.hasProperty("Match")).thenReturn(true);
        when(definition.getProperty("Match")).thenReturn("2");

        ValidationRule rule = new FindValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("D"),
            this.factory.create("E")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertFalse(rule.validateDataset(null));
    }

    /**
     * Assertion: If Test matches somewhere in the dataset, a Find rule should pass.
     */
    @Test
    public void OneTestMatchSuccess() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Test")).thenReturn(true);
        when(definition.getProperty("Test")).thenReturn(".*");

        ValidationRule rule = new FindValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("B"),
            this.factory.create("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }

    /**
     * Assertion: The If clause should be able to check multiple independent conditions.
     */
    @Test
    public void MultipleIfMatchesSuccess() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("C");
        when(definition.hasProperty("If")).thenReturn(true);
        when(definition.getProperty("If")).thenReturn("XX == 'A' @iand XX == 'B'");

        ValidationRule rule = new FindValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("B"),
            this.factory.create("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }

    /**
     * Assertion: The If clause should be able to check multiple independent conditions.
     */
    @Test
    public void MultipleIfMatchesIgnore() throws ConfigurationException, CorruptRuleException {
        RuleDefinition definition = mock(RuleDefinition.class);

        when(definition.getProperty(anyString())).thenReturn("");
        when(definition.getId()).thenReturn("XX");
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn("XX");
        when(definition.hasProperty("Terms")).thenReturn(true);
        when(definition.getProperty("Terms")).thenReturn("Q");
        when(definition.hasProperty("If")).thenReturn(true);
        when(definition.getProperty("If")).thenReturn("XX == 'A' @iand XX == 'D'");

        ValidationRule rule = new FindValidationRule(definition, this.session, MOCK_SCOPE);

        List<DataRecord> records = ValidationRuleTestHelper.createRecords("XX",
            this.factory.create("A"),
            this.factory.create("B"),
            this.factory.create("C")
        );

        for (DataRecord record : records) {
            rule.validate(record);
        }

        assertTrue(rule.validateDataset(null));
    }
}

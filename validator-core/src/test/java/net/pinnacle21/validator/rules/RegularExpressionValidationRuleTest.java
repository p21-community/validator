/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules;

import java.util.List;
import java.util.Random;

import net.pinnacle21.validator.api.model.CancellationToken;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.ValidationSession;
import net.pinnacle21.validator.settings.RuleDefinition;
import net.pinnacle21.validator.settings.ConfigurationException;
import static net.pinnacle21.validator.rules.ValidationRuleTestHelper.MOCK_SCOPE;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class RegularExpressionValidationRuleTest {
    private static final String DURATION_FORMAT = "^(R\\d*/)?P(?:\\d+(?:\\.\\d+)?Y)?(?:\\d+(?:\\.\\d+)?M)?(?:\\d+(?:\\.\\d+)?W)?(?:\\d+(?:\\.\\d+)?D)?(?:T(?:\\d+(?:\\.\\d+)?H)?(?:\\d+(?:\\.\\d+)?M)?(?:\\d+(?:\\.\\d+)?S)?)?$";
    private static final Random RANDOM = new Random();

    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);
    private final ValidationSession session = ValidationSession.create(this.options, new CancellationToken(),
            this.factory, null);

    @Test
    public void testISO8691() throws ConfigurationException, CorruptRuleException {
        String variable = "TEST_DUR";
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(variable,
            this.factory.create("P80Y"), // simple, one-unit (year)
            this.factory.create("P1Y2M10DT2H30M"), // complex, multi-unit (year, month, day, hour, minute)
            this.factory.create("P1.5Y2.5M10.5DT2.5H30.5M") // complex, multi-unit, with fractions
        );

        RuleDefinition definition = mock(RuleDefinition.class);
        when(definition.getId()).thenReturn("XX");
        when(definition.getRuleType()).thenReturn("Regex");
        when(definition.getType()).thenReturn(Diagnostic.Type.Error);
        when(definition.hasProperty("Variable")).thenReturn(true);
        when(definition.getProperty("Variable")).thenReturn(variable);
        when(definition.hasProperty("Test")).thenReturn(true);
        when(definition.getProperty("Test")).thenReturn(DURATION_FORMAT);

        RegularExpressionValidationRule rule = new RegularExpressionValidationRule(definition, this.session, MOCK_SCOPE);

        for (DataRecord record : records) {
            assertTrue(rule.validate(record));
        }

        // negative testing
        byte[] randomBytes = new byte[13];
        RANDOM.nextBytes(randomBytes);
        List<DataRecord> randomData =
                ValidationRuleTestHelper.createRecords("TEST_DUR", this.factory.create(new String(randomBytes)));

        for (DataRecord random : randomData) {
            assertFalse(rule.validate(random));
        }
    }
}

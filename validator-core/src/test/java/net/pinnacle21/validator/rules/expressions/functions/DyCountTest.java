/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.rules.expressions.functions;

import static org.junit.Assert.*;

import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.data.DataEntryFactory;
import org.junit.Test;
import net.pinnacle21.validator.data.DataEntry;
import net.pinnacle21.validator.data.DataRecord;
import net.pinnacle21.validator.rules.ValidationRuleTestHelper;

import java.util.List;

/**
 * @author Tim Stone
 */
public class DyCountTest {
    private final ValidationOptions options = ValidationOptions.builder().build();
    private final DataEntryFactory factory = new DataEntryFactory(this.options);

    /**
     * Assertion: A recorded date ahead of the reference date should have the expected
     *     DY value days + 1
     */
    @Test
    public void PositiveDateIsCorrect() {
        String[] names = new String[] { "REF", "REC" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] { this.factory.create("2013-03-01"), this.factory.create("2013-03-02") }
        );
        Function function = new DyCount("DY", this.factory, names);

        assertEquals(this.factory.create(2), function.compute(records.get(0)));
    }

    /**
     * Assertion: A recorded date ahead of the reference date should have the expected
     *     DY value days + 1, irrespective of time
     */
    @Test
    public void PositiveDateIsCorrectWithDateTimes() {
        String[] names = new String[] { "REF", "REC" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] { this.factory.create("2013-03-01T01:24:57"), this.factory.create("2013-03-02T23:19:41") }
        );
        Function function = new DyCount("DY", this.factory, names);

        assertEquals(this.factory.create(2), function.compute(records.get(0)));
    }

    /**
     * Assertion: A recorded date before the reference date should have the expected
     *     DY value -days
     */
    @Test
    public void NegativeDateIsCorrect() {
        String[] names = new String[] { "REF", "REC" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] { this.factory.create("2013-03-02"), this.factory.create("2013-03-01") }
        );
        Function function = new DyCount("DY", this.factory, names);

        assertEquals(this.factory.create(-1), function.compute(records.get(0)));
    }

    /**
     * Assertion: A recorded date that's the same as the reference day should have DY
     *     value 1
     */
    @Test
    public void SameDayIsCorrect() {
        String[] names = new String[] { "REF", "REC" };
        List<DataRecord> records = ValidationRuleTestHelper.createRecords(
            names,
            new DataEntry[] { this.factory.create("2013-03-01"), this.factory.create("2013-03-01") }
        );
        Function function = new DyCount("DY", this.factory, names);

        assertEquals(this.factory.create(1), function.compute(records.get(0)));
    }
}

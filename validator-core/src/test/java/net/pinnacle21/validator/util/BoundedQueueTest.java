/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.util;

import static org.junit.Assert.*;

import java.util.Queue;

import org.junit.Test;

/**
 * @author Tim Stone
 */
public class BoundedQueueTest {
    /**
     * Ensures that the capacity set for a BoundedQueue is not exceeded when the queue is
     * set to not auto-shift
     */
    @Test
    public void CapacityNotExceededNoShift() {
        Queue<Object> queue = new BoundedQueue<>(2, false);

        assertTrue(queue.offer(new Object()));
        assertTrue(queue.offer(new Object()));
        assertFalse(queue.offer(new Object()));
        assertEquals(2, queue.size());
    }

    /**
     * Ensures that the capacity set for a BoundedQueue is not exceeded when the queue is
     * set to auto-shift elements
     */
    @Test
    public void CapacityNotExceededAutoShift() {
        Queue<Object> queue = new BoundedQueue<>(2, true);

        assertTrue(queue.offer(new Object()));
        assertTrue(queue.offer(new Object()));
        assertTrue(queue.offer(new Object()));
        assertEquals(2, queue.size());
    }
}

/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.util;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Tim Stone
 */
public class HelpersTest {
    /**
     * Assertion: Our LIS algorithm is correctly implemented
     */
    @Test
    public void LIS() {
        int[] input, actual, expected;

        input = new int[] { 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 };
        actual = Helpers.determineLargestIncreasingSubsequence(input);
        expected = new int[] { 0, 2, 6, 9, 11, 15 };

        assertArrayEquals(expected, actual);

        input = new int[] { 1, 5, 2, 3, 4, 7, 6 };
        actual = Helpers.determineLargestIncreasingSubsequence(input);
        expected = new int[] { 1, 2, 3, 4, 6 };

        assertArrayEquals(expected, actual);

        input = new int[] { 2, 1, 3, 4 };
        actual = Helpers.determineLargestIncreasingSubsequence(input);
        expected = new int[] { 1, 3, 4 };

        assertArrayEquals(expected, actual);

        input = new int[] { 1, 5, 2, 3, 4, 7, 6 };
        actual = Helpers.determineLargestIncreasingSubsequence(input, true);
        expected = new int[] { 0, 2, 3, 4, 6 };

        assertArrayEquals(expected, actual);
    }
}

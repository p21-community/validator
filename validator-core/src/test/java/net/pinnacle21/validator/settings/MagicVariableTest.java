/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;
import net.pinnacle21.validator.settings.Definition.Target;
import net.pinnacle21.validator.settings.MagicVariableParser.MagicProperty;

/**
 *
 * @author Tim Stone
 */
public class MagicVariableTest {
    /**
     *
     */
    @Test
    public void SimplePropertyDoesMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables$Define.WithCodeList%",
            Collections.emptyList(),
            Collections.singletonList("Define.WithCodeList")
        );
        Definition definition = new Definition(Target.Variable, "LBBLFL");

        definition.setProperty("Define.WithCodeList", "Y");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void SimplePropertyDoesNotMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables$Define.WithCodeList%",
            Collections.emptyList(),
            Collections.singletonList("Define.WithCodeList")
        );
        Definition definition = new Definition(Target.Variable, "LBBLFL");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void SimpleClauseDoesMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY]%",
            Collections.singletonList(Collections.singletonList("*DY")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "AESTDY");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void SimpleClauseDoesNotMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY]%",
            Collections.singletonList(Collections.singletonList("*DY")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "AESTDTC");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void ComplexPropertyDoesMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables$Type:DateTime|Date|Time%",
            Collections.emptyList(),
            Collections.singletonList("Type:DateTime|Date|Time")
        );
        Definition definition = new Definition(Target.Variable, "LBDTC");

        definition.setProperty("Type", "Time");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void ComplexPropertyDoesNotMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables$Type:DateTime|Date|Time%",
            Collections.emptyList(),
            Collections.singletonList("Type:DateTime|Date|Time")
        );
        Definition definition = new Definition(Target.Variable, "LBBLFL");

        definition.setProperty("Type", "Text");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiPropertyDoesMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables$Type:Time$CodeList%",
            Collections.emptyList(),
            Arrays.asList("Type:Time", "CodeList")
        );
        Definition definition = new Definition(Target.Variable, "LBDTC");

        definition.setProperty("Type", "Time");
        definition.setProperty("CodeList", "Y");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiPropertyDoesNotMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables$Type:Time$Define%",
            Collections.emptyList(),
            Arrays.asList("Type:Time", "Define")
        );
        Definition definition = new Definition(Target.Variable, "LBDTC");

        definition.setProperty("Type", "Time");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiSingleClauseDoesMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY,*DTC]%",
            Collections.singletonList(Arrays.asList("*DY", "*DTC")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "AESTDTC");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiSingleClauseDoesNotMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY,*DTC]%",
            Collections.singletonList(Arrays.asList("*DY", "*DTC")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "LBBLFL");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void NegativeClauseDoesMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY]%",
            Collections.singletonList(Collections.singletonList("!*DY")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "AESTDTC");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void NegativeClauseDoesNotMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY]%",
            Collections.singletonList(Collections.singletonList("!*DY")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "AESTDY");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiMatchClauseDoesMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY,*DTC]%",
            Arrays.asList(Collections.singletonList("*DTC"), Collections.singletonList("AE*")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "AESTDTC");

        assertTrue(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void MultiMatchClauseDoesNoMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY,*DTC]%",
            Arrays.asList(Collections.singletonList("*DTC"), Collections.singletonList("AE*")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "LBSTDTC");

        assertFalse(variable.matches(definition));
    }

    /**
     *
     */
    @Test
    public void ClauseReferences() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY,*DTC]%",
            Collections.singletonList(Collections.singletonList("TR##SDTM")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "TR01SDTM");

        assertTrue(variable.matches(definition));

        Map<Integer, String> references = variable.getReferences(definition);

        assertFalse(references.isEmpty());
        assertEquals("01", references.get(1));
    }

    /**
     *
     */
    @Test
    public void MultiClauseReferences() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables[*DY,*DTC]%",
            Collections.singletonList(Collections.singletonList("TR##AG#N")),
            Collections.emptyList()
        );
        Definition definition = new Definition(Target.Variable, "TR01AG1N");

        assertTrue(variable.matches(definition));

        Map<Integer, String> references = variable.getReferences(definition);

        assertFalse(references.isEmpty());
        assertEquals("01", references.get(1));
        assertEquals("1", references.get(2));
    }

    @Test
    public void RegexPropertyValueCheckMatches() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables$Length:/\\d+/%",
            Collections.emptyList(),
            Collections.singletonList("Length:/\\d+/")
        );
        Definition definition = new Definition(Target.Variable, "LBDTC");

        definition.setProperty("Length", "8");

        assertTrue(variable.matches(definition));
    }

    @Test
    public void RegexPropertyValueCheckDoesntMatch() {
        MagicVariable variable = new MagicVariable(
            MagicProperty.Variables,
            "%Variables$Length:/\\d+/%",
            Collections.emptyList(),
            Collections.singletonList("Length:/\\d+/")
        );
        Definition definition = new Definition(Target.Variable, "LBDTC");

        definition.setProperty("Length", " 8");

        assertFalse(variable.matches(definition));
    }
}

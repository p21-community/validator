/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validator.settings;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class TemplateTest {
    @Test
    public void canMatchOnPlainVariables() {
        Template template = new Template("ABC", new PrototypeCriteria("", new String[] { "A" }));

        assertEquals(10, template.matches("XYZ", Collections.singleton("A")));
        assertEquals(0, template.matches("XYZ", Collections.singleton("B")));
    }

    @Test
    public void canMatchBasedOnName() {
        Template template = new Template("ABC", new PrototypeCriteria("ABC*", new String[] {}));

        assertEquals(1, template.matches("ABCDEF", Collections.singleton("A")));
        assertEquals(0, template.matches("XYZ", Collections.singleton("B")));
    }

    @Test
    public void negatedVariableExcludesMatch() {
        Template template = new Template("ABC", new PrototypeCriteria("", new String[] { "A", "-B" }));

        assertEquals(10, template.matches("XYZ", Arrays.asList("A", "C")));
        assertEquals(0, template.matches("XYZ", Arrays.asList("A", "B")));
    }

    @Test
    public void canMatchOnWildcardVariables() {
        Template template = new Template("ABC", new PrototypeCriteria("", new String[] { "B*" }));

        assertEquals(10, template.matches("XYZ", Arrays.asList("BC", "CB")));
    }

    @Test
    public void negatedVariableExcludesRegexMatch() {
        Template template = new Template("ABC", new PrototypeCriteria("", new String[] { "-B*" }));

        assertEquals(0, template.matches("XYZ", Collections.singleton("BC")));
    }
}
